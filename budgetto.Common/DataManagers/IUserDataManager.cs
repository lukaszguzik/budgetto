﻿using budgetto.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace budgetto.Common.DataManagers
{
    public interface IUserDataManager
    {
        Task<UserDetailedModel> AddUserAsync(UserDetailedModel newUser);
        Task<T> GetUserAsync<T>(int id) where T : UserModel;
        Task<T> GetUserAsync<T>(string email) where T : UserModel;
        Task<bool> UserExistsAsync(string email);
    }
}
