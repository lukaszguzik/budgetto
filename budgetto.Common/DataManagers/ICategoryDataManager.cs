﻿using budgetto.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace budgetto.Common.DataManagers
{
    public interface ICategoryDataManager
    {
        Task<CategoryModel> GetCategoryAsync(int walletId, int id, CategoryTypeModel type);
        Task<CategoryModel> AddCategoryAsync(int walletId, CategoryModel expenseCategory, CategoryTypeModel type);
        Task<IEnumerable<CategoryModel>> GetAllCategoriesAsync(int walletId, CategoryTypeModel type);
        Task UpdateCategoryAsync(int walletId, int id, CategoryModel expenseCategory, CategoryTypeModel type);
    }
}
