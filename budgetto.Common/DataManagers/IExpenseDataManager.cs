﻿using budgetto.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace budgetto.Common.DataManagers
{
    public interface IExpenseDataManager
    {
        Task<ExpenseModel> GetExpenseAsync(int walletId, int id);
        Task<ExpenseModel> AddExpenseAsync(int walletId, ExpenseModel expense);
        Task<ExpensesModel> GetExpensesAsync(int walletId,
            int pageNo, int countOnPage,
            OrderTypeModel orderType,
            string name = null,
            decimal? minAmount = null, decimal? maxAmount = null,
            DateTimeOffset? minDate = null, DateTimeOffset? maxDate = null,
            List<int> categoryIds = null);
        Task UpdateExpenseAsync(int walletId, int id, ExpenseModel expense);
    }
}
