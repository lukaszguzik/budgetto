﻿using budgetto.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace budgetto.Common.DataManagers
{
    public interface IWalletDataManager
    {
        #region Wallet data operations
        Task<WalletModel> AddWalletAsync(WalletModel newWallet);
        Task<WalletModel> GetWalletAsync(int walletId);
        Task<IEnumerable<WalletModel>> GetAllOwnedWalletsAsync(int ownerId);
        Task<IEnumerable<WalletModel>> GetAllSharedWalletsAsync(int userId);
        Task<AllWalletsModel> GetAllWalletsAsync(int userId);
        #endregion

        #region ShareInvite data operations
        Task<ShareInviteModel> AddShareInviteAsync(int walletId, ShareInviteModel sendShareInviteModel);
        Task<ShareInviteModel> GetShareInviteAsync(int shareInviteId);
        Task<ShareInviteModel> GetShareInviteAsync(Guid acceptToken);
        Task<IEnumerable<ShareInviteModel>> GetAllShareInvitesForWalletAsync(int walletId);
        Task<IEnumerable<ShareInviteModel>> GetAllShareInvitesForUserAsync(int invitedUserId);
        Task<ShareInviteModel> GetShareInviteForUserAndWalletAsync(int invitedUserId, int walletId);
        Task UpdateShareInviteAsync(int shareInviteId, ShareInviteModel updatedModel);
        #endregion
    }
}
