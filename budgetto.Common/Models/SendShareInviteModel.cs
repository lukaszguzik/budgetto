﻿using budgetto.Common.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace budgetto.Common.Models
{
    public class SendShareInviteModel : BaseModel
    {
        /// <summary>
        /// Email of User you want to share wallet with
        /// </summary>
        public string UserEmail { get; set; }

        /// <summary>
        /// If true invited user will have readonly access
        /// </summary>
        public bool IsReadOnly { get; set; }

        /// <summary>
        /// How long invite should be valid
        /// </summary>
        public DateTimeOffset InviteValidTo { get; set; }
    }
}
