﻿using budgetto.Common.Models.Helpers;
using budgetto.Common.Resources;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.Common.Models.Validation
{
    public class ExpenseModelValidation : AbstractValidator<ExpenseModel>
    {
        public ExpenseModelValidation()
        {
            RuleSet("Create", () =>
            {
                RuleFor(m => m.Name).NotEmpty().WithLocalizedMessage(typeof(ErrorMessages), nameof(ErrorMessages.NameRequired));
                RuleFor(m => m.Name).MaximumLength(ModelRestrictions.StandardNameMaxLength).WithLocalizedMessage(typeof(ErrorMessages), nameof(ErrorMessages.NameTooLong));

                RuleFor(m => m.Amount).NotEqual(0).WithLocalizedMessage(typeof(ErrorMessages), nameof(ErrorMessages.AmountInvalid));

                RuleFor(m => m.Date).NotEmpty().WithLocalizedMessage(typeof(ErrorMessages), nameof(ErrorMessages.DateInvalid));
            });
        }
    }
}
