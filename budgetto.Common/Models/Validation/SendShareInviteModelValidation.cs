﻿using budgetto.Common.Models.Helpers;
using budgetto.Common.Resources;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace budgetto.Common.Models.Validation
{
    public class SendShareInviteModelValidation : AbstractValidator<SendShareInviteModel>
    {
        public SendShareInviteModelValidation()
        {
            RuleFor(m => m.UserEmail).NotEmpty().WithLocalizedMessage(typeof(ErrorMessages), nameof(ErrorMessages.EmailRequired));
            RuleFor(m => m.UserEmail).MaximumLength(ModelRestrictions.EmailMaxLength).WithLocalizedMessage(typeof(ErrorMessages), nameof(ErrorMessages.EmailTooLong));
            RuleFor(m => m.UserEmail).EmailAddress().WithLocalizedMessage(typeof(ErrorMessages), nameof(ErrorMessages.EmailInvalid));

            RuleFor(m => m.InviteValidTo).GreaterThan(DateTimeOffset.Now).WithLocalizedMessage(typeof(ErrorMessages), nameof(ErrorMessages.ShareInviteValidToInPast));
        }
    }
}
