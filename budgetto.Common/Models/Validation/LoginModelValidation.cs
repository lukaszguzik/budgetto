﻿using budgetto.Common.Models.Helpers;
using budgetto.Common.Resources;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace budgetto.Common.Models.Validation
{
    public class LoginModelValidation : AbstractValidator<LoginModel>
    {
        public LoginModelValidation()
        {
            RuleFor(m => m.Email).NotEmpty().WithLocalizedMessage(typeof(ErrorMessages), nameof(ErrorMessages.EmailRequired));
            RuleFor(m => m.Email).MaximumLength(ModelRestrictions.EmailMaxLength).WithLocalizedMessage(typeof(ErrorMessages), nameof(ErrorMessages.EmailTooLong));
            RuleFor(m => m.Email).EmailAddress().WithLocalizedMessage(typeof(ErrorMessages), nameof(ErrorMessages.EmailInvalid));

            RuleFor(m => m.Password).NotEmpty().WithLocalizedMessage(typeof(ErrorMessages), nameof(ErrorMessages.PasswordRequired));
        }
    }
}
