﻿using budgetto.Common.Models.Helpers;
using budgetto.Common.Resources;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.Common.Models.Validation
{
    public class WalletModelValidation : AbstractValidator<WalletModel>
    {
        public WalletModelValidation()
        {
            RuleFor(m => m.Name).NotEmpty().WithLocalizedMessage(typeof(ErrorMessages), nameof(ErrorMessages.NameRequired));
            RuleFor(m => m.Name).MaximumLength(ModelRestrictions.StandardNameMaxLength).WithLocalizedMessage(typeof(ErrorMessages), nameof(ErrorMessages.NameTooLong));
        }
    }
}
