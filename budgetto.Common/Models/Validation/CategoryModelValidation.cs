﻿using budgetto.Common.Models.Helpers;
using budgetto.Common.Resources;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.Common.Models.Validation
{
    public class CategoryModelValidation : AbstractValidator<CategoryModel>
    {
        public CategoryModelValidation()
        {
            RuleSet("Create", () =>
            {
                RuleFor(m => m.Name).NotEmpty().WithLocalizedMessage(typeof(ErrorMessages), nameof(ErrorMessages.NameRequired));
                RuleFor(m => m.Name).MaximumLength(ModelRestrictions.StandardNameMaxLength).WithLocalizedMessage(typeof(ErrorMessages), nameof(ErrorMessages.NameTooLong));

                RuleFor(m => m.Color).Matches(ModelRestrictions.ColorRegex).WithLocalizedMessage(typeof(ErrorMessages), nameof(ErrorMessages.ColorInvalid));
            });
        }
    }
}
