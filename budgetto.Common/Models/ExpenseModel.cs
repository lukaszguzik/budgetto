﻿using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.Common.Models
{
    public class ExpenseModel : BaseModel
    {
        /// <summary>
        /// Expense ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Expense name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Expense amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Expense date
        /// </summary>
        public DateTimeOffset Date { get; set; }

        /// <summary>
        /// Expense category IDs
        /// </summary>
        public List<int> CategoryIds { get; set; }
    }
}
