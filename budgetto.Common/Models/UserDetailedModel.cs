﻿using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.Common.Models
{
    public class UserDetailedModel : UserModel
    {
        /// <summary>
        /// User password hash
        /// </summary>
        public byte[] PasswordHash { get; set; }

        /// <summary>
        /// User password salt
        /// </summary>
        public byte[] PasswordSalt { get; set; }

        /// <summary>
        /// Date and time of token issue from which they are honoured
        /// </summary>
        public DateTimeOffset? HonourTokensIssuedAfter { get; set; }
    }
}
