﻿using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.Common.Models
{
    public class LoginResponseModel
    {
        /// <summary>
        /// JWT authorization token
        /// </summary>
        public string AuthToken { get; set; }

        /// <summary>
        /// JWT refresh token
        /// </summary>
        public string RefreshToken { get; set; }

        /// <summary>
        /// JWT authorization token expiration date and time
        /// </summary>
        public DateTimeOffset AuthTokenExpirationDateTime { get; set; }

        /// <summary>
        /// Logged in user details
        /// </summary>
        public UserModel User { get; set; }

        /// <summary>
        /// User won't be logout after some time of inactivity
        /// </summary>
        public bool DoNotLogout { get; set; }
    }
}
