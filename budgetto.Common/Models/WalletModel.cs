﻿using budgetto.Common.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace budgetto.Common.Models
{
    public class WalletModel : BaseModel
    {
        /// <summary>
        /// Wallet unique id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Wallet name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Wallet owner Id
        /// </summary>
        public int OwnerId { get; set; }

        /// <summary>
        /// Wallet owner name
        /// </summary>
        public string OwnerName { get; set; }
    }
}
