﻿using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.Common.Models
{
    public class RefreshTokenModel : BaseModel
    {
        /// <summary>
        /// User won't be logout after some time of inactivity
        /// </summary>
        public bool DoNotLogout { get; set; }
    }
}
