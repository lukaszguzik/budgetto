﻿using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.Common.Models.Helpers
{
    public static class ModelRestrictions
    {
        public const int StandardNameMaxLength = 50;
        public const int EmailMaxLength = 254;
        //public const string EmailRegex = @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";
        public const string ColorRegex = @"^[a-fA-F0-9]{6}$";
    }
}
