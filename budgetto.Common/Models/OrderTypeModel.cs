﻿using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.Common.Models
{
    public enum OrderTypeModel : int
    {
        NameAscending = 1,
        NameDescending = 2,
        AmountAscending = 3,
        AmountDescending = 4,
        DateAscending = 5,
        DateDescending = 6
    }
}
