﻿using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.Common.Models
{
    public enum CategoryTypeModel : int
    {
        ExpenseCategory = 1,
        IncomeCategory = 2
    }
}
