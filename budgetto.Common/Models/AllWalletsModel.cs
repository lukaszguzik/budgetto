﻿using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.Common.Models
{
    public class AllWalletsModel : BaseModel
    {
        /// <summary>
        /// All wallets owned by some user
        /// </summary>
        public IEnumerable<WalletModel> OwnedWallets { get; set; }

        /// <summary>
        /// All wallets shared to some user
        /// </summary>
        public IEnumerable<WalletModel> SharedWallets { get; set; }
    }
}
