﻿using budgetto.Common.Models.Helpers;
using budgetto.Common.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace budgetto.Common.Models
{
    public class LoginModel : BaseModel
    {
        /// <summary>
        /// User email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// User password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// User won't be logout after some time of inactivity
        /// </summary>
        public bool DoNotLogout { get; set; }
    }
}
