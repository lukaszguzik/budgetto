﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.Common.Models
{
    public class BaseModel
    {
        public override string ToString()
        {
            return $"{base.ToString()}:{JsonConvert.SerializeObject(this)}";
        }
    }
}
