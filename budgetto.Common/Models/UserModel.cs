﻿using budgetto.Common.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace budgetto.Common.Models
{
    public class UserModel
    {
        /// <summary>
        /// User unique ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// User email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// User password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// User name
        /// </summary>
        [Required]
        [StringLength(ModelRestrictions.StandardNameMaxLength)]
        public string Name { get; set; }
    }
}
