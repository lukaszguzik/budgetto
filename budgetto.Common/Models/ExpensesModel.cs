﻿using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.Common.Models
{
    public class ExpensesModel : BaseModel
    {
        /// <summary>
        /// List of expenses
        /// </summary>
        public IEnumerable<ExpenseModel> Expenses { get; set; }

        /// <summary>
        /// Total expenses count
        /// </summary>
        public int Total { get; set; }

        /// <summary>
        /// Total page count
        /// </summary>
        public int PageCount { get; set; }
    }
}
