﻿using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.Common.Models
{
    public class ShareInviteModel
    {
        /// <summary>
        /// ShareInvite ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Shared wallet ID
        /// </summary>
        public int WalletId { get; set; }

        /// <summary>
        /// Invited user ID
        /// </summary>
        public int InvitedUserId { get; set; }

        /// <summary>
        /// If true invited user will have readonly access
        /// </summary>
        public bool IsReadOnly { get; set; }

        /// <summary>
        /// Is invite accepted
        /// </summary>
        public bool Accepted { get; set; }

        /// <summary>
        /// Is invite active
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Token for invite acceptance
        /// </summary>
        public Guid AcceptToken { get; set; }

        /// <summary>
        /// Accept token validity date and time
        /// </summary>
        public DateTimeOffset AcceptTokenValidTo { get; set; }
    }
}
