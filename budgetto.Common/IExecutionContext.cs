﻿using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.Common
{
    public interface IExecutionContext
    {
        int? GetExecutingUserId();
        string GetExecutingUserName();
    }
}
