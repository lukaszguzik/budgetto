﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace budgetto.Common.Exceptions
{
    public class BudgettoException : Exception
    {
        public ErrorsModel Errors { get; set; }

        public BudgettoException()
        {
        }

        public BudgettoException(ErrorsModel errors)
        {
            Errors = errors;
        }

        public BudgettoException(string message, ErrorsModel errors) : base(message)
        {
            Errors = errors;
        }

        public BudgettoException(string message, Exception innerException, ErrorsModel errors) : base(message, innerException)
        {
            Errors = errors;
        }

        protected BudgettoException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
