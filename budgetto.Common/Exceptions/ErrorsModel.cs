﻿using budgetto.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.Common.Exceptions
{
    public class ErrorsModel : BaseModel
    {
        public Dictionary<string, List<string>> ErrorMessages { get; set; }

        public ErrorsModel()
        {
            ErrorMessages = new Dictionary<string, List<string>>();
        }

        public ErrorsModel(params string[] messages) : this()
        {
            ErrorMessages[CommonConstValues.KeyForErrorsWithoutField] = new List<string>(messages);
        }
    }
}
