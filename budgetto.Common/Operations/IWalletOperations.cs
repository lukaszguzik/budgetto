﻿using budgetto.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace budgetto.Common.Operations
{
    public interface IWalletOperations
    {
        #region Wallet operations
        Task<WalletModel> CreateWalletAsync(WalletModel newWallet);
        Task<WalletModel> GetWalletAsync(int walletId);
        Task<IEnumerable<WalletModel>> GetAllOwnedWalletsAsync();
        Task<IEnumerable<WalletModel>> GetAllSharedWalletsAsync();
        Task<AllWalletsModel> GetAllWalletsAsync();
        #endregion

        #region Category operations
        Task<CategoryModel> CreateCategoryAsync(int walletId, CategoryModel category, CategoryTypeModel type);
        Task<CategoryModel> GetCategoryAsync(int walletId, int id, CategoryTypeModel type);
        Task<IEnumerable<CategoryModel>> GetAllCategoriesAsync(int walletId, CategoryTypeModel type);
        Task EditCategoryAsync(int walletId, int id, CategoryModel category, CategoryTypeModel type);
        #endregion

        #region Expense opertions
        Task<ExpenseModel> CreateExpenseAsync(int walletId, ExpenseModel expense);
        Task<ExpenseModel> GetExpenseAsync(int walletId, int id);
        Task<ExpensesModel> GetExpensesAsync(int walletId,
            int pageNo, int countOnPage,
            OrderTypeModel orderType,
            string name = null,
            decimal? minAmount = null, decimal? maxAmount = null,
            DateTimeOffset? minDate = null, DateTimeOffset? maxDate = null,
            List<int> categoryIds = null);
        Task UpdateExpenseAsync(int walletId, int id, ExpenseModel expense);
        #endregion

        #region ShareInvite operations
        Task<SendShareInviteModel> SendShareInviteAsync(int walletId, SendShareInviteModel sendShareInviteModel);
        Task<IEnumerable<ShareInviteModel>> GetShareInvitesForWalletAsync(int walletId);
        Task<IEnumerable<ShareInviteModel>> GetShareInvitesForUserAsync();
        Task AcceptShareInviteAsync(Guid acceptToken);
        Task RevokeShareInviteAsync(int shareInviteId);
        #endregion
    }
}
