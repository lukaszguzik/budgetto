﻿using budgetto.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace budgetto.Common.Operations
{
    public interface IUserOperations
    {
        Task<UserModel> RegisterUserAsync(UserModel newUser);
        Task<T> GetUserAsync<T>(int id) where T : UserModel;
        Task<UserModel> LoginUserAsync(LoginModel loginModel);
    }
}
