﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace budgetto.DAL.AutoMapperConfiguration
{
    internal static class Mapper
    {
        private static MapperConfiguration config;

        static Mapper()
        {
            config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfiles(Assembly.GetExecutingAssembly());
            });

            config.AssertConfigurationIsValid();
        }

        static public IMapper Instance
        {
            get
            {
                return config.CreateMapper();
            }
        }
    }
}
