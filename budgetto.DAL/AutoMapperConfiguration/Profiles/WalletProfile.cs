﻿using AutoMapper;
using budgetto.Common.Models;
using budgetto.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.DAL.AutoMapperConfiguration.Profiles
{
    class WalletProfile : Profile
    {
        public WalletProfile()
        {
            CreateMap<Wallet, WalletModel>()
                .ForMember(dst => dst.OwnerName, opt => opt.ResolveUsing(src => src.Owner?.Name))
                .ReverseMap()
                .ForMember(dst => dst.Owner, opt => opt.Ignore())
                .ForMember(dst => dst.Category, opt => opt.Ignore());
        }
    }
}
