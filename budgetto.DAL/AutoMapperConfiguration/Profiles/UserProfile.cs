﻿using AutoMapper;
using budgetto.Common.Models;
using budgetto.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.DAL.AutoMapperConfiguration.Profiles
{
    class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserModel>()
                .ForMember(dst => dst.Password, opt => opt.Ignore())
                .ReverseMap()
                .ForMember(dst => dst.PasswordHash, opt => opt.Ignore())
                .ForMember(dst => dst.PasswordSalt, opt => opt.Ignore());
        }
    }
}
