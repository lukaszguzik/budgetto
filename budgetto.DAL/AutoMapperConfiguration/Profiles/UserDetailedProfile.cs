﻿using AutoMapper;
using budgetto.Common.Models;
using budgetto.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.DAL.AutoMapperConfiguration.Profiles
{
    class UserDetailedProfile : Profile
    {
        public UserDetailedProfile()
        {
            CreateMap<User, UserDetailedModel>()
                .ForMember(dst => dst.Password, opt => opt.Ignore())
               .ReverseMap();

            CreateMap<UserModel, UserDetailedModel>()
                .ForMember(dst => dst.PasswordHash, opt => opt.Ignore())
                .ForMember(dst => dst.PasswordSalt, opt => opt.Ignore())
                .ForMember(dst => dst.HonourTokensIssuedAfter, opt => opt.Ignore())
                .ReverseMap();
        }
    }
}
