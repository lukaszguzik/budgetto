﻿using AutoMapper;
using budgetto.Common.Models;
using budgetto.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.DAL.AutoMapperConfiguration.Profiles
{
    class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            CreateMap<Category, CategoryModel>();

            CreateMap<CategoryModel, Category>()
                .ForMember(dst => dst.Id, opt => opt.Ignore())
                .ForMember(dst => dst.Name, opt => opt.ResolveUsing((dst, src) => string.IsNullOrWhiteSpace(src.Name) ? dst.Name : src.Name))
                .ForMember(dst => dst.Color, opt => opt.ResolveUsing((dst, src) => string.IsNullOrWhiteSpace(src.Color) ? dst.Color : src.Color))
                .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}
