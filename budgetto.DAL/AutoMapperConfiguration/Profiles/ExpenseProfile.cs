﻿using AutoMapper;
using budgetto.Common.Models;
using budgetto.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace budgetto.DAL.AutoMapperConfiguration.Profiles
{
    class ExpenseProfile : Profile
    {
        public ExpenseProfile()
        {
            CreateMap<Expense, ExpenseModel>()
                .ForMember(dst => dst.CategoryIds, opt => opt.ResolveUsing((src, dst) => dst.CategoryIds = src.ExpenseCategory.Select(ec => ec.CategoryId).ToList()));

            CreateMap<ExpenseModel, Expense>()
                .ForMember(dst => dst.Id, opt => opt.Ignore())
                .ForMember(dst => dst.Name, opt => opt.ResolveUsing((src, dst) => string.IsNullOrWhiteSpace(src.Name) ? dst.Name : src.Name))
                .ForMember(dst => dst.WalletId, opt => opt.Ignore())
                .ForMember(dst => dst.Wallet, opt => opt.Ignore())
                .ForMember(dst => dst.ExpenseCategory, opt => opt.Ignore());
        }
    }
}
