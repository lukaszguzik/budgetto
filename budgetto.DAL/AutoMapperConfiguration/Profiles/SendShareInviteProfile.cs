﻿using AutoMapper;
using budgetto.Common.Models;
using budgetto.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace budgetto.DAL.AutoMapperConfiguration.Profiles
{
    class SendShareInviteProfile : Profile
    {
        public SendShareInviteProfile()
        {
            CreateMap<ShareInvite, ShareInviteModel>()
               .ReverseMap();
        }
    }
}
