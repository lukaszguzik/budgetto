﻿using System;
using System.Collections.Generic;

namespace budgetto.DAL.Models
{
    public partial class CategoryType
    {
        public CategoryType()
        {
            Category = new HashSet<Category>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Category> Category { get; set; }
    }
}
