﻿using System;
using System.Collections.Generic;

namespace budgetto.DAL.Models
{
    public partial class Expense
    {
        public Expense()
        {
            ExpenseCategory = new HashSet<ExpenseCategory>();
        }

        public int Id { get; set; }
        public int WalletId { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public DateTimeOffset Date { get; set; }

        public Wallet Wallet { get; set; }
        public ICollection<ExpenseCategory> ExpenseCategory { get; set; }
    }
}
