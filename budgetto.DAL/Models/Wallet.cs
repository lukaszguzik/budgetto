﻿using System;
using System.Collections.Generic;

namespace budgetto.DAL.Models
{
    public partial class Wallet
    {
        public Wallet()
        {
            Category = new HashSet<Category>();
            Expense = new HashSet<Expense>();
            ShareInvite = new HashSet<ShareInvite>();
        }

        public int Id { get; set; }
        public int OwnerId { get; set; }
        public string Name { get; set; }

        public User Owner { get; set; }
        public ICollection<Category> Category { get; set; }
        public ICollection<Expense> Expense { get; set; }
        public ICollection<ShareInvite> ShareInvite { get; set; }
    }
}
