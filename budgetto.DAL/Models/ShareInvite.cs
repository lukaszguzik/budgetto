﻿using System;
using System.Collections.Generic;

namespace budgetto.DAL.Models
{
    public partial class ShareInvite
    {
        public int Id { get; set; }
        public int WalletId { get; set; }
        public int InvitedUserId { get; set; }
        public bool IsReadOnly { get; set; }
        public bool Accepted { get; set; }
        public bool Active { get; set; }
        public Guid? AcceptToken { get; set; }
        public DateTimeOffset? AcceptTokenValidTo { get; set; }

        public User InvitedUser { get; set; }
        public Wallet Wallet { get; set; }
    }
}
