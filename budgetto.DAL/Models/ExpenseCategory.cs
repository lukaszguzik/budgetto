﻿using System;
using System.Collections.Generic;

namespace budgetto.DAL.Models
{
    public partial class ExpenseCategory
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public int ExpenseId { get; set; }

        public Category Category { get; set; }
        public Expense Expense { get; set; }
    }
}
