﻿using System;
using System.Collections.Generic;

namespace budgetto.DAL.Models
{
    public partial class Category
    {
        public Category()
        {
            ExpenseCategory = new HashSet<ExpenseCategory>();
        }

        public int Id { get; set; }
        public int WalletId { get; set; }
        public int CategoryTypeId { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }

        public CategoryType CategoryType { get; set; }
        public Wallet Wallet { get; set; }
        public ICollection<ExpenseCategory> ExpenseCategory { get; set; }
    }
}
