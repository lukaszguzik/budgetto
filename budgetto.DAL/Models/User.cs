﻿using System;
using System.Collections.Generic;

namespace budgetto.DAL.Models
{
    public partial class User
    {
        public User()
        {
            ShareInvite = new HashSet<ShareInvite>();
            Wallet = new HashSet<Wallet>();
        }

        public int Id { get; set; }
        public string Email { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string Name { get; set; }
        public DateTimeOffset? HonourTokensIssuedAfter { get; set; }

        public ICollection<ShareInvite> ShareInvite { get; set; }
        public ICollection<Wallet> Wallet { get; set; }
    }
}
