﻿using budgetto.Common.DataManagers;
using budgetto.Common.Models;
using budgetto.DAL.AutoMapperConfiguration;
using budgetto.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace budgetto.DAL.DataManagers
{
    public class CategoryDataManager : BaseDataManager, ICategoryDataManager
    {
        public CategoryDataManager(BudgettoContext dbContext)
            : base(dbContext)
        {
        }

        public async virtual Task<CategoryModel> AddCategoryAsync(int walletId, CategoryModel category, CategoryTypeModel type)
        {
            var categoryEntity = Mapper.Instance.Map<Category>(category);
            categoryEntity.WalletId = walletId;
            categoryEntity.CategoryTypeId = (int)type;

            await dbContext.Category.AddAsync(categoryEntity);
            await dbContext.SaveChangesAsync();

            return Mapper.Instance.Map<CategoryModel>(categoryEntity);
        }

        public async virtual Task<CategoryModel> GetCategoryAsync(int walletId, int id, CategoryTypeModel type)
        {
            var categoryEntity = await dbContext.Category.SingleAsync(c => c.WalletId == walletId && c.Id == id && c.CategoryTypeId == (int)type);

            return Mapper.Instance.Map<CategoryModel>(categoryEntity);
        }

        public async virtual Task<IEnumerable<CategoryModel>> GetAllCategoriesAsync(int walletId, CategoryTypeModel type)
        {
            var categoryEntities = await dbContext.Category.Where(c => c.WalletId == walletId && c.CategoryTypeId == (int)type).ToListAsync();

            return Mapper.Instance.Map<IEnumerable<CategoryModel>>(categoryEntities);
        }

        public async virtual Task UpdateCategoryAsync(int walletId, int id, CategoryModel category, CategoryTypeModel type)
        {
            var categoryEntity = await dbContext.Category.SingleAsync(c => c.WalletId == walletId && c.Id == id && c.CategoryTypeId == (int)type);
            Mapper.Instance.Map(category, categoryEntity);
            await dbContext.SaveChangesAsync();
        }
    }
}
