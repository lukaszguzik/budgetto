﻿using budgetto.Common.DataManagers;
using System;
using System.Collections.Generic;
using System.Text;
using budgetto.Common.Models;
using budgetto.DAL.Models;
using budgetto.DAL.AutoMapperConfiguration;
using System.Security.Cryptography;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace budgetto.DAL.DataManagers
{
    public class UserDataManager : BaseDataManager, IUserDataManager
    {
        public UserDataManager(BudgettoContext dbContext)
            : base(dbContext)
        {
        }

        public async virtual Task<UserDetailedModel> AddUserAsync(UserDetailedModel newUser)
        {
            var userEntity = Mapper.Instance.Map<User>(newUser);

            await dbContext.User.AddAsync(userEntity);
            await dbContext.SaveChangesAsync();

            return Mapper.Instance.Map<UserDetailedModel>(userEntity);
        }

        public async virtual Task<T> GetUserAsync<T>(int id) where T : UserModel
        {
            var userEntity = await dbContext.User.AsNoTracking().SingleAsync(u => u.Id == id);

            return Mapper.Instance.Map<T>(userEntity);
        }

        public async virtual Task<T> GetUserAsync<T>(string email) where T : UserModel
        {
            var userEntity = await dbContext.User.AsNoTracking().SingleAsync(u => u.Email == email);

            return Mapper.Instance.Map<T>(userEntity);
        }

        public async Task<bool> UserExistsAsync(string email)
        {
            return await dbContext.User.AsNoTracking().AnyAsync(u => u.Email == email);
        }
    }
}
