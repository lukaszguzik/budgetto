﻿using budgetto.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace budgetto.DAL.DataManagers
{
    public class BaseDataManager
    {
        protected BudgettoContext dbContext;

        public BaseDataManager(BudgettoContext dbContext)
        {
            this.dbContext = dbContext;
        }
    }
}
