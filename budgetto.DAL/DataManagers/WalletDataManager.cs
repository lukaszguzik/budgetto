﻿using System;
using System.Collections.Generic;
using System.Text;
using budgetto.DAL.Models;
using budgetto.Common.Models;
using System.Threading.Tasks;
using budgetto.DAL.AutoMapperConfiguration;
using budgetto.Common.DataManagers;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace budgetto.DAL.DataManagers
{
    public class WalletDataManager : BaseDataManager, IWalletDataManager
    {
        public WalletDataManager(BudgettoContext dbContext) : base(dbContext)
        {
        }

        #region Wallet data operations
        public async virtual Task<WalletModel> AddWalletAsync(WalletModel newWallet)
        {
            var walletEntity = Mapper.Instance.Map<Wallet>(newWallet);
            await dbContext.Wallet.AddAsync(walletEntity);
            await dbContext.SaveChangesAsync();

            return Mapper.Instance.Map<WalletModel>(walletEntity);
        }

        public async virtual Task<WalletModel> GetWalletAsync(int walletId)
        {
            var walletEntity = await dbContext.Wallet
                .Include(w => w.Owner)
                .AsNoTracking()
                .SingleAsync(w => w.Id == walletId);

            return Mapper.Instance.Map<WalletModel>(walletEntity);
        }

        public async Task<IEnumerable<WalletModel>> GetAllOwnedWalletsAsync(int ownerId)
        {
            var walletsList = await dbContext.Wallet
                .Include(w => w.Owner)
                .AsNoTracking()
                .Where(w => w.OwnerId == ownerId)
                .ToListAsync();

            return Mapper.Instance.Map<IEnumerable<WalletModel>>(walletsList);
        }

        public async Task<IEnumerable<WalletModel>> GetAllSharedWalletsAsync(int userId)
        {
            var walletsList = await dbContext.ShareInvite
                .Include(si => si.Wallet.Owner)
                .AsNoTracking()
                .Where(si => si.InvitedUserId == userId && si.Accepted && si.Active)
                .Select(si => si.Wallet)
                .ToListAsync();

            return Mapper.Instance.Map<IEnumerable<WalletModel>>(walletsList);
        }

        public async Task<AllWalletsModel> GetAllWalletsAsync(int userId)
        {
            return new AllWalletsModel
            {
                OwnedWallets = await GetAllOwnedWalletsAsync(userId),
                SharedWallets = await GetAllSharedWalletsAsync(userId)
            };
        }
        #endregion

        #region ShareInvite data operations
        public async Task<ShareInviteModel> AddShareInviteAsync(int walletId, ShareInviteModel sendShareInviteModel)
        {
            var walletEntity = await dbContext.Wallet.SingleAsync(w => w.Id == walletId);
            var shareInviteEntity = Mapper.Instance.Map<ShareInvite>(sendShareInviteModel);
            shareInviteEntity.WalletId = walletId;
            await dbContext.ShareInvite.AddAsync(shareInviteEntity);
            await dbContext.SaveChangesAsync();

            return Mapper.Instance.Map<ShareInviteModel>(shareInviteEntity);
        }

        public async Task<ShareInviteModel> GetShareInviteAsync(int shareInviteId)
        {
            var shareInviteEntity = await dbContext.ShareInvite.AsNoTracking().SingleAsync(si => si.Id == shareInviteId);
            return Mapper.Instance.Map<ShareInviteModel>(shareInviteEntity);
        }

        public async Task<ShareInviteModel> GetShareInviteAsync(Guid acceptToken)
        {
            var shareInviteEntity = await dbContext.ShareInvite.AsNoTracking().SingleAsync(si => si.AcceptToken == acceptToken);
            return Mapper.Instance.Map<ShareInviteModel>(shareInviteEntity);
        }

        public async Task<IEnumerable<ShareInviteModel>> GetAllShareInvitesForWalletAsync(int walletId)
        {
            var shareInvitesList = await dbContext.ShareInvite
                .AsNoTracking()
                .Where(si => si.WalletId == walletId)
                .ToListAsync();

            return Mapper.Instance.Map<IEnumerable<ShareInviteModel>>(shareInvitesList);
        }

        public async Task<IEnumerable<ShareInviteModel>> GetAllShareInvitesForUserAsync(int invitedUserId)
        {
            var shareInvitesList = await dbContext.ShareInvite
                .AsNoTracking()
                .Where(si => si.InvitedUserId == invitedUserId)
                .ToListAsync();

            return Mapper.Instance.Map<IEnumerable<ShareInviteModel>>(shareInvitesList);
        }

        public async Task<ShareInviteModel> GetShareInviteForUserAndWalletAsync(int invitedUserId, int walletId)
        {
            var shareInvitesList = await dbContext.ShareInvite
                .AsNoTracking()
                .Where(si => si.InvitedUserId == invitedUserId && si.WalletId == walletId)
                .SingleAsync();

            return Mapper.Instance.Map<ShareInviteModel>(shareInvitesList);
        }

        public async Task UpdateShareInviteAsync(int shareInviteId, ShareInviteModel updatedModel)
        {
            updatedModel.Id = shareInviteId;
            var shareInviteEntity = await dbContext.ShareInvite.SingleAsync(si => si.Id == shareInviteId);
            Mapper.Instance.Map(updatedModel, shareInviteEntity);
            await dbContext.SaveChangesAsync();
        }
        #endregion
    }
}
