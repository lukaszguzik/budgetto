﻿using budgetto.Common.DataManagers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using budgetto.Common.Models;
using budgetto.DAL.Models;
using budgetto.DAL.AutoMapperConfiguration;
using Microsoft.EntityFrameworkCore;

namespace budgetto.DAL.DataManagers
{
    public class ExpenseDataManager : BaseDataManager, IExpenseDataManager
    {
        public ExpenseDataManager(BudgettoContext dbContext)
            : base(dbContext)
        {
        }

        public async virtual Task<ExpenseModel> AddExpenseAsync(int walletId, ExpenseModel expense)
        {
            var expenseEntity = Mapper.Instance.Map<Expense>(expense);
            expenseEntity.WalletId = walletId;

            await dbContext.Expense.AddAsync(expenseEntity);
            if (expense.CategoryIds != null)
            {
                foreach (var categoryId in expense.CategoryIds)
                {
                    expenseEntity.ExpenseCategory.Add(new ExpenseCategory { CategoryId = categoryId });
                }
            }
            await dbContext.SaveChangesAsync();

            return Mapper.Instance.Map<ExpenseModel>(expenseEntity);
        }

        public async virtual Task<ExpenseModel> GetExpenseAsync(int walletId, int id)
        {
            var expenseEntity = await dbContext.Expense.Include(e => e.ExpenseCategory).SingleAsync(e => e.WalletId == walletId && e.Id == id );

            return Mapper.Instance.Map<ExpenseModel>(expenseEntity);
        }

        public async virtual Task<ExpensesModel> GetExpensesAsync(int walletId,
            int pageNo, int countOnPage,
            OrderTypeModel orderType,
            string name = null,
            decimal? minAmount = null, decimal? maxAmount = null,
            DateTimeOffset? minDate = null, DateTimeOffset? maxDate = null,
            List<int> categoryIds = null)
        {
            var query = dbContext.Expense.Include(e => e.ExpenseCategory).Where(e => e.WalletId == walletId);
            if(!string.IsNullOrEmpty(name))
            {
                query = query.Where(e => e.Name.Contains(name));
            }

            if (minAmount != null)
            {
                query = query.Where(e => e.Amount >= minAmount);
            }
            if (maxAmount != null)
            {
                query = query.Where(e => e.Amount <= maxAmount);
            }

            if (minDate != null)
            {
                query = query.Where(e => e.Date >= minDate);
            }
            if (maxDate != null)
            {
                query = query.Where(e => e.Date <= maxDate);
            }

            if(categoryIds != null && categoryIds.Count > 0)
            {
                query = query.Where(e => e.ExpenseCategory.Any(ec => categoryIds.Contains(ec.CategoryId)));
            }

            switch(orderType)
            {
                case OrderTypeModel.NameAscending:
                    query = query.OrderBy(e => e.Name);
                    break;

                case OrderTypeModel.NameDescending:
                    query = query.OrderByDescending(e => e.Name);
                    break;

                case OrderTypeModel.AmountAscending:
                    query = query.OrderBy(e => e.Amount);
                    break;

                case OrderTypeModel.AmountDescending:
                    query = query.OrderByDescending(e => e.Amount);
                    break;

                case OrderTypeModel.DateAscending:
                    query = query.OrderBy(e => e.Date);
                    break;

                case OrderTypeModel.DateDescending:
                    query = query.OrderByDescending(e => e.Date);
                    break;
            }

            var result = new ExpensesModel
            {
                Total = await query.CountAsync(),
            };
            result.PageCount = (result.Total + countOnPage - 1) / countOnPage;

            query = query.Skip((pageNo - 1) * countOnPage).Take(countOnPage);

            result.Expenses = Mapper.Instance.Map<IEnumerable<ExpenseModel>>(await query.ToListAsync());

            return result;
        }

        public async virtual Task UpdateExpenseAsync(int walletId, int id, ExpenseModel expense)
        {
            var expenseEntity = await dbContext.Expense.Include(e => e.ExpenseCategory).SingleAsync(e => e.WalletId == walletId && e.Id == id);
            Mapper.Instance.Map(expense, expenseEntity);
            foreach(var ec in expenseEntity.ExpenseCategory)
            {
                if(!expense.CategoryIds.Contains(ec.CategoryId))
                {
                    expenseEntity.ExpenseCategory.Remove(ec);
                    dbContext.ExpenseCategory.Remove(ec);
                }
            }

            foreach (var ecId in expense.CategoryIds)
            {
                if(!expenseEntity.ExpenseCategory.Any(ec => ec.CategoryId == ecId))
                {
                    expenseEntity.ExpenseCategory.Add(new ExpenseCategory { CategoryId = ecId });
                }
            }

            await dbContext.SaveChangesAsync();
        }
    }
}
