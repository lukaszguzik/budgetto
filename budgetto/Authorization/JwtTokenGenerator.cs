﻿using budgetto.Common.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace budgetto.Authorization
{
    public class JwtTokenGenerator : IJwtTokenGenerator
    {
        private readonly JwtIssuerOptions jwtOptions;

        public JwtTokenGenerator(IOptions<JwtIssuerOptions> jwtOptions)
        {
            this.jwtOptions = jwtOptions.Value;
        }

        public JwtTokenModel CreateToken(UserModel loggedUser, bool createRefreshToken, bool refreshTokenExpires)
        {
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, loggedUser.Id.ToString()),
                new Claim(ClaimTypes.Name, loggedUser.Name),
                new Claim(JwtRegisteredClaimNames.Email, loggedUser.Email),
                new Claim(JwtRegisteredClaimNames.Iat, DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString()),
                new Claim(PrivateClaimTypes.IsRefreshToken, createRefreshToken.ToString())
            };

            DateTimeOffset? tokenExpiration;
            if(createRefreshToken && !refreshTokenExpires)
            {
                tokenExpiration = null;
            }
            else
            {
                tokenExpiration = DateTimeOffset.Now.Add(createRefreshToken ? jwtOptions.RefreshTokenValidFor : jwtOptions.AuthTokenValidFor);
            }

            var token = new JwtSecurityToken(
                        issuer: jwtOptions.Issuer,
                        audience: jwtOptions.Audience,
                        claims: claims,
                        notBefore: DateTime.UtcNow,
                        expires: tokenExpiration?.UtcDateTime,
                        signingCredentials: jwtOptions.SigningCredentials
                    );

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(token);

            return new JwtTokenModel
            {
                Token = encodedJwt,
                ExpirationDateTime = tokenExpiration
            };
        }
    }
}
