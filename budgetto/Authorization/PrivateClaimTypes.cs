﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace budgetto.Authorization
{
    public static class PrivateClaimTypes
    {
        public const string IsRefreshToken = "IsRefreshToken";
    }
}
