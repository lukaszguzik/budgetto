﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace budgetto.Authorization
{
    public class NotRefreshTokenRequirementHandler : AuthorizationHandler<NotRefreshTokenRequirement>
    {
        protected readonly ILogger<NotRefreshTokenRequirementHandler> logger;

        public NotRefreshTokenRequirementHandler(ILogger<NotRefreshTokenRequirementHandler> logger)
        {
            this.logger = logger;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, NotRefreshTokenRequirement requirement)
        {
            if (context.User.Identity.IsAuthenticated) //chek only when user is authenticated 
            {
                if (context.User.HasClaim(c => c.Type == PrivateClaimTypes.IsRefreshToken) //has IsRefreshToken claim
                    && bool.Parse(context.User.FindFirstValue(PrivateClaimTypes.IsRefreshToken)) //and it is set to true
                    )
                {
                    throw new UnauthorizedAccessException();
                }

                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}
