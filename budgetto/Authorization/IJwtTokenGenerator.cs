﻿using budgetto.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace budgetto.Authorization
{
    public interface IJwtTokenGenerator
    {
        JwtTokenModel CreateToken(UserModel loggedUser, bool createRefreshToken, bool refreshTokenExpires);
    }
}
