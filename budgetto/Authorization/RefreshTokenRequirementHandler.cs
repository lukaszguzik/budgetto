﻿using budgetto.Common.Models;
using budgetto.Common.Operations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace budgetto.Authorization
{
    public class RefreshTokenRequirementHandler : AuthorizationHandler<RefreshTokenRequirement>
    {
        protected readonly ILogger<RefreshTokenRequirementHandler> logger;
        protected IUserOperations userOperations;

        public RefreshTokenRequirementHandler(ILogger<RefreshTokenRequirementHandler> logger, IUserOperations userOperations)
        {
            this.logger = logger;
            this.userOperations = userOperations;
        }

        protected async override Task HandleRequirementAsync(AuthorizationHandlerContext context, RefreshTokenRequirement requirement)
        {
            if (context.User.Identity.IsAuthenticated) //chek only when user is authenticated 
            {
                if (context.User.HasClaim(c => c.Type == JwtRegisteredClaimNames.Iat) //has Iat claim
                    && context.User.HasClaim(c => c.Type == PrivateClaimTypes.IsRefreshToken) //and has IsRefreshToken claim
                    && bool.Parse(context.User.FindFirstValue(PrivateClaimTypes.IsRefreshToken)) //and it is refreshToken
                    )
                {
                    if (int.TryParse(context.User.FindFirstValue(JwtRegisteredClaimNames.Iat), out int iat))
                    {
                        var userId = int.Parse(context.User.FindFirst(c => c.Type == ClaimTypes.NameIdentifier).Value);
                        var userDetailed = await userOperations.GetUserAsync<UserDetailedModel>(userId);

                        if (userDetailed.HonourTokensIssuedAfter == null || userDetailed.HonourTokensIssuedAfter <= DateTimeOffset.FromUnixTimeSeconds(iat))
                        {
                            context.Succeed(requirement);
                            return;
                        }
                    }
                }

                throw new UnauthorizedAccessException(); //jeżeli nie powiodło się sprawdzenie ważności tokana rzucamy wyjątek
            }
        }
    }
}
