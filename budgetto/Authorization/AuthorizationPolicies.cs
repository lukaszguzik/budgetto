﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace budgetto.Authorization
{
    public static class AuthorizationPolicies
    {
        public const string RefreshTokenPolicy = "RefreshTokenPolicy";
        public const string AuthTokenPolicy = "AuthTokenPolicy";
    }
}
