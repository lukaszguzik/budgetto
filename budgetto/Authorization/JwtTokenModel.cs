﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace budgetto.Authorization
{
    public class JwtTokenModel
    {
        public string Token { get; set; }
        public DateTimeOffset? ExpirationDateTime { get; set; }
    }
}
