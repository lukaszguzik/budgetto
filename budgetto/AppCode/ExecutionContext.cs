﻿using budgetto.Common;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace budgetto.AppCode
{
    public class ExecutionContext : IExecutionContext
    {
        protected IHttpContextAccessor httpContextAccessor;

        public ExecutionContext(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        public int? GetExecutingUserId()
        {
            return GetValueFromClaims<int?>(ClaimTypes.NameIdentifier);
        }

        public string GetExecutingUserName()
        {
            return GetValueFromClaims<string>(ClaimTypes.Name);
        }

        private T GetValueFromClaims<T>(string claimType)
        {
            T value = default(T);
            var user = httpContextAccessor.HttpContext.User;
            if (user != null && user.HasClaim(c => c.Type == claimType))
            {
                var claimValue = user.FindFirst(c => c.Type == claimType).Value;
                if(!string.IsNullOrEmpty(claimValue))
                {
                    var type = typeof(T);
                    type = Nullable.GetUnderlyingType(type) ?? type;
                    value = (T)Convert.ChangeType(claimValue, type);
                }
            }

            return value;
        }
    }
}
