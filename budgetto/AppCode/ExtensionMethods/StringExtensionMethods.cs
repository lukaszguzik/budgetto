﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace budgetto.AppCode.ExtensionMethods
{
    public static class StringExtensionMethods
    {
        public static string PascalToCamelCase(this string input)
        {
            string output = input;
            if (input.Length > 0)
            {
                var firstLetter = char.ToLower(input[0]);
                if (input.Length == 1)
                {
                    output = firstLetter.ToString();
                }
                else
                {
                    output = firstLetter + input.Substring(1);
                }
            }

            return output;
        }
    }
}
