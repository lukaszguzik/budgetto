﻿using budgetto.Common.Exceptions;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace budgetto.AppCode.ExtensionMethods
{
    public static class ModelStateExcensionMethods
    {
        public static ErrorsModel ToErrorsModel(this ModelStateDictionary modelStateDictionary)
        {
            return new ErrorsModel
            {
                ErrorMessages = modelStateDictionary.Where(msd => msd.Value.Errors.Count > 0)
                    .ToDictionary(msd => msd.Key.PascalToCamelCase(), msd => msd.Value.Errors.Select(e => e.ErrorMessage).ToList())
            };
        }
    }
}
