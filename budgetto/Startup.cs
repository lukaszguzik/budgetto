﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using budgetto.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;
using NLog.Web;
using NLog.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.PlatformAbstractions;
using System.IO;
using System.Text;
using budgetto.Middleware;
using Microsoft.AspNetCore.Http;
using budgetto.Common;
using budgetto.AppCode;
using FluentValidation.AspNetCore;

namespace budgetto
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        private readonly SymmetricSecurityKey signingKey;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();

            signingKey = JwtSecurityKey.Create(Configuration.GetValue<string>("BUDGETTO_JWT_SECRET"));
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Get options from app settings
            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));

            // Configure JwtIssuerOptions
            services.Configure<JwtIssuerOptions>(options =>
            {
                options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
                options.SigningCredentials = options.SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
                if (TimeSpan.TryParse(jwtAppSettingOptions[nameof(JwtIssuerOptions.AuthTokenValidFor)], out TimeSpan validFor))
                {
                    options.AuthTokenValidFor = validFor;
                }

                if (TimeSpan.TryParse(jwtAppSettingOptions[nameof(JwtIssuerOptions.RefreshTokenValidFor)], out validFor))
                {
                    options.RefreshTokenValidFor = validFor;
                }
            });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = signingKey,

                    ValidateIssuer = true,
                    ValidIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)],

                    ValidateAudience = true,
                    ValidAudience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)],

                    RequireExpirationTime = false,
                    ValidateLifetime = true,

                    ClockSkew = TimeSpan.Zero
                };
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy(AuthorizationPolicies.RefreshTokenPolicy, policy => policy
                    .RequireAuthenticatedUser()
                    .AddRequirements(new RefreshTokenRequirement()));

                options.AddPolicy(AuthorizationPolicies.AuthTokenPolicy, policy => policy
                    .RequireAuthenticatedUser()
                    .AddRequirements(new NotRefreshTokenRequirement()));
            });

            services.AddMvc()
            .AddJsonOptions(options =>
            {
                options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            })
            .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Common.Models.Validation.LoginModelValidation>());

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "budgetto API", Version = "v1" });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });

                // Set the comments path for the Swagger JSON and UI.
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "budgetto.xml");
                c.IncludeXmlComments(Path.Combine(basePath, "budgetto.xml"));
                c.IncludeXmlComments(Path.Combine(basePath, "budgetto.Common.xml"));

                c.DescribeAllEnumsAsStrings();
            });

            ConfigureDependencyInjection(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseExceptionMiddleware();
            }
            else
            {
                app.UseExceptionMiddleware();
                app.UseExceptionHandler();
            }

            env.ConfigureNLog("nlog.config");
            loggerFactory.AddNLog();
            app.AddNLogWeb();

            app.UseAuthentication();

            //app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "budgetto API");
            });

            app.UseMvc(routes =>
            {
                routes.MapSpaFallbackRoute("spa-fallback", new { controller = "Home", action = "Index" });
            });
        }

        private void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //Authorization
            services.AddScoped<IAuthorizationHandler, NotRefreshTokenRequirementHandler>();
            services.AddScoped<IAuthorizationHandler, RefreshTokenRequirementHandler>();
            services.AddSingleton<IJwtTokenGenerator, JwtTokenGenerator>();

            //ExecutionContext
            services.AddSingleton<IExecutionContext, ExecutionContext>();

            //DbContext
            services.AddDbContext<budgetto.DAL.Models.BudgettoContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("BudgettoDatabase")));

            //Data managers
            services.AddScoped<budgetto.Common.DataManagers.IUserDataManager, budgetto.DAL.DataManagers.UserDataManager>();
            services.AddScoped<budgetto.Common.DataManagers.IWalletDataManager, budgetto.DAL.DataManagers.WalletDataManager>();
            services.AddScoped<budgetto.Common.DataManagers.ICategoryDataManager, budgetto.DAL.DataManagers.CategoryDataManager>();
            services.AddScoped<budgetto.Common.DataManagers.IExpenseDataManager, budgetto.DAL.DataManagers.ExpenseDataManager>();

            //Operations
            services.AddScoped<budgetto.Common.Operations.IUserOperations, budgetto.BLL.Operations.UserOperations>();
            services.AddScoped<budgetto.Common.Operations.IWalletOperations, budgetto.BLL.Operations.WalletOperations>();
        }
    }
}
