﻿using budgetto.AppCode.ExtensionMethods;
using budgetto.Authorization;
using budgetto.Common.Exceptions;
using budgetto.Common.Models;
using budgetto.Common.Operations;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace budgetto.Controllers.Api
{
    [Authorize(Policy = AuthorizationPolicies.AuthTokenPolicy)]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class WalletController : BaseController
    {
        protected readonly ILogger<WalletController> logger;

        protected IWalletOperations walletOperations;

        public WalletController(ILogger<WalletController> logger, IWalletOperations walletOperations)
        {
            this.logger = logger;
            this.walletOperations = walletOperations;
        }

        #region Wallet
        /// <summary>
        /// Adds new wallet for logged in user
        /// </summary>
        /// <remarks>Field `Wallet.Id` is ignored</remarks>
        /// <param name="wallet">New wallet data</param>
        /// <response code="201">Returns the newly-created wallet</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(WalletModel), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost()]
        public async virtual Task<IActionResult> CreateWallet([FromBody] WalletModel wallet)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var createdWallet = await walletOperations.CreateWalletAsync(wallet);
                    return CreatedAtAction("GetWallet", new { id = createdWallet.Id }, createdWallet);
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                return HandleStandardExceptions(ex, logger);
            }
        }

        /// <summary>
        /// Gets wallet data
        /// </summary>
        /// <param name="id">Wallet id</param>
        /// <response code="200">Returns wallet from system</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(WalletModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{id:int}")]
        public async virtual Task<IActionResult> GetWallet(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var wallet = await walletOperations.GetWalletAsync(id);
                    return Ok(wallet);
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                return HandleStandardExceptions(ex, logger);
            }
        }

        /// <summary>
        /// Gets all wallets owned by logged user
        /// </summary>
        /// <response code="200">Returns list of wallets from system</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(WalletModel[]), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("[action]")]
        public async virtual Task<IActionResult> GetAllOwnedWallets()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var wallets = await walletOperations.GetAllOwnedWalletsAsync();
                    return Ok(wallets);
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                return HandleStandardExceptions(ex, logger);
            }
        }

        /// <summary>
        /// Gets all shared wallets to logged user
        /// </summary>
        /// <response code="200">Returns list of wallets from system</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(WalletModel[]), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("[action]")]
        public async virtual Task<IActionResult> GetAllSharedWallets()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var wallets = await walletOperations.GetAllSharedWalletsAsync();
                    return Ok(wallets);
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                return HandleStandardExceptions(ex, logger);
            }
        }

        /// <summary>
        /// Gets all wallets (owned and shared) to logged user
        /// </summary>
        /// <response code="200">Returns list of wallets from system</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(AllWalletsModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("[action]")]
        public async virtual Task<IActionResult> GetAllWallets()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var wallets = await walletOperations.GetAllWalletsAsync();
                    return Ok(wallets);
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                return HandleStandardExceptions(ex, logger);
            }
        }
        #endregion

        #region Expense categories
        /// <summary>
        /// Gets expense category by ID
        /// </summary>
        /// <param name="walletId">Wallet ID</param>
        /// <param name="id">Expense category ID</param>
        /// <response code="200">Returns expense category data</response>
        /// <response code="400">If the model is invalid</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(CategoryModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{walletId:int}/ExpenseCategory/{id:int}")]
        public async virtual Task<IActionResult> GetExpenseCategory(int walletId, int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var expenseCategory = await walletOperations.GetCategoryAsync(walletId, id, CategoryTypeModel.ExpenseCategory);
                    return Ok(expenseCategory);
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                return HandleStandardExceptions(ex, logger);
            }
        }

        /// <summary>
        /// Gets all expense categories by wallet ID
        /// </summary>
        /// <param name="walletId">Expense category ID</param>
        /// <response code="200">Returns expense category data</response>
        /// <response code="400">If the model is invalid</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(CategoryModel[]), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{walletId:int}/ExpenseCategory")]
        public async virtual Task<IActionResult> GetAllExpenseCategories(int walletId)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var expenseCategories = await walletOperations.GetAllCategoriesAsync(walletId, CategoryTypeModel.ExpenseCategory);
                    return Ok(expenseCategories);
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                return HandleStandardExceptions(ex, logger);
            }
        }

        /// <summary>
        /// Creates new expense category in wallet
        /// </summary>
        /// <param name="walletId">Wallet ID</param>
        /// <param name="category">New expense category data</param>
        /// <response code="200">Returns added expense category data</response>
        /// <response code="400">If the model is invalid</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(CategoryModel), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost("{walletId:int}/ExpenseCategory")]
        public async virtual Task<IActionResult> CreateExpenseCategory(int walletId, [CustomizeValidator(RuleSet = "Create")][FromBody] CategoryModel category)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var createdExpenseCategory = await walletOperations.CreateCategoryAsync(walletId, category, CategoryTypeModel.ExpenseCategory);
                    return CreatedAtAction("GetExpenseCategory", new { id = createdExpenseCategory.Id }, createdExpenseCategory);
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                return HandleStandardExceptions(ex, logger);
            }
        }
        #endregion

        #region Income categories
        /// <summary>
        /// Gets income category by ID
        /// </summary>
        /// <param name="walletId">Wallet ID</param>
        /// <param name="id">Income category ID</param>
        /// <response code="200">Returns income category data</response>
        /// <response code="400">If the model is invalid</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(CategoryModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{walletId:int}/IncomeCategory/{id:int}")]
        public async virtual Task<IActionResult> GetIncomeCategory(int walletId, int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var expenseCategory = await walletOperations.GetCategoryAsync(walletId, id, CategoryTypeModel.IncomeCategory);
                    return Ok(expenseCategory);
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                return HandleStandardExceptions(ex, logger);
            }
        }

        /// <summary>
        /// Gets all income categories by wallet ID
        /// </summary>
        /// <param name="walletId">Income category ID</param>
        /// <response code="200">Returns income category data</response>
        /// <response code="400">If the model is invalid</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(CategoryModel[]), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{walletId:int}/IncomeCategory")]
        public async virtual Task<IActionResult> GetAllIncomeCategories(int walletId)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var expenseCategories = await walletOperations.GetAllCategoriesAsync(walletId, CategoryTypeModel.IncomeCategory);
                    return Ok(expenseCategories);
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                return HandleStandardExceptions(ex, logger);
            }
        }

        /// <summary>
        /// Creates new income category in wallet
        /// </summary>
        /// <param name="walletId">Wallet ID</param>
        /// <param name="category">New income category data</param>
        /// <response code="200">Returns added income category data</response>
        /// <response code="400">If the model is invalid</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(CategoryModel), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost("{walletId:int}/IncomeCategory")]
        public async virtual Task<IActionResult> CreateIncomeCategory(int walletId, [CustomizeValidator(RuleSet = "Create")][FromBody] CategoryModel category)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var createdExpenseCategory = await walletOperations.CreateCategoryAsync(walletId, category, CategoryTypeModel.IncomeCategory);
                    return CreatedAtAction("GetIncomeCategory", new { id = createdExpenseCategory.Id }, createdExpenseCategory);
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                return HandleStandardExceptions(ex, logger);
            }
        }
        #endregion

        #region Expenses
        /// <summary>
        /// Gets expense by ID
        /// </summary>
        /// <param name="walletId">Wallet ID</param>
        /// <param name="id">Expense ID</param>
        /// <response code="200">Returns expense data</response>
        /// <response code="400">If the model is invalid</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(ExpenseModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{walletId:int}/Expense/{id:int}")]
        public async virtual Task<IActionResult> GetExpense(int walletId, int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var expense = await walletOperations.GetExpenseAsync(walletId, id);
                    return Ok(expense);
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                return HandleStandardExceptions(ex, logger);
            }
        }

        /// <summary>
        /// Gets all expense categories by wallet ID
        /// </summary>
        /// <param name="walletId">Expense category ID</param>
        /// <param name="pageNo">Page number</param>
        /// <param name="countOnPage">Number of records on page</param>
        /// <param name="orderType">Type of ordering</param>
        /// <param name="name">Part of expense name</param>
        /// <param name="minAmount">Minimal expense amount</param>
        /// <param name="maxAmount">Maximal expense amount</param>
        /// <param name="minDate">Minimal expense date</param>
        /// <param name="maxDate">Maximal expense date</param>
        /// <param name="categoryIds">List of categories ids</param>
        /// <response code="200">Returns expense category data</response>
        /// <response code="400">If the model is invalid</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(ExpensesModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{walletId:int}/Expense")]
        public async virtual Task<IActionResult> GetExpenses(int walletId,
            int pageNo, int countOnPage,
            OrderTypeModel orderType,
            string name = null,
            decimal? minAmount = null, decimal? maxAmount = null,
            DateTimeOffset? minDate = null, DateTimeOffset? maxDate = null,
            List<int> categoryIds = null)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var expenses = await walletOperations.GetExpensesAsync(walletId, pageNo, countOnPage, orderType, name, minAmount, maxAmount, minDate, maxDate, categoryIds);
                    return Ok(expenses);
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                return HandleStandardExceptions(ex, logger);
            }
        }

        /// <summary>
        /// Creates new expense in wallet
        /// </summary>
        /// <param name="walletId">Wallet ID</param>
        /// <param name="expense">New expense data</param>
        /// <response code="200">Returns added expense data</response>
        /// <response code="400">If the model is invalid</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(ExpenseModel), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost("{walletId:int}/Expense")]
        public async virtual Task<IActionResult> CreateExpense(int walletId, [CustomizeValidator(RuleSet = "Create")][FromBody] ExpenseModel expense)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var createdExpense = await walletOperations.CreateExpenseAsync(walletId, expense);
                    return CreatedAtAction("GetExpense", new { id = createdExpense.Id }, createdExpense);
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                return HandleStandardExceptions(ex, logger);
            }
        }
        #endregion

        #region Sharing wallets
        /// <summary>
        /// Sends share invite to specified user
        /// </summary>
        /// <response code="200">Returns share invite data</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(ShareInviteModel[]), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost("{walletId:int}/[action]")]
        public async virtual Task<IActionResult> SendShareInvite(int walletId, [FromBody] SendShareInviteModel sendShareInvite)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var invite = await walletOperations.SendShareInviteAsync(walletId, sendShareInvite);
                    return Ok(invite);
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                return HandleStandardExceptions(ex, logger);
            }
        }

        /// <summary>
        /// Get share invites for specified wallet
        /// </summary>
        /// <response code="200">Returns list of share invites</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(ShareInviteModel[]), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{walletId:int}/ShareInvites")]
        public async virtual Task<IActionResult> GetShareInvitesForWallet(int walletId)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var invites = await walletOperations.GetShareInvitesForWalletAsync(walletId);
                    return Ok(invites);
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                return HandleStandardExceptions(ex, logger);
            }
        }

        /// <summary>
        /// Get share invites for logged user
        /// </summary>
        /// <response code="200">Returns list of share invites</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(ShareInviteModel[]), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("ShareInvitesForUser")]
        public async virtual Task<IActionResult> GetShareInvitesForUser()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var invites = await walletOperations.GetShareInvitesForUserAsync();
                    return Ok(invites);
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                return HandleStandardExceptions(ex, logger);
            }
        }

        /// <summary>
        /// Accepts share invite
        /// </summary>
        /// <response code="204">No content</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost("[action]/{acceptToken:Guid}")]
        public async virtual Task<IActionResult> AcceptShareInvite(Guid acceptToken)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await walletOperations.AcceptShareInviteAsync(acceptToken);
                    return NoContent();
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                return HandleStandardExceptions(ex, logger);
            }
        }

        /// <summary>
        /// Revokes share invite
        /// </summary>
        /// <response code="204">No content</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost("[action]/{shareInviteId:int}")]
        public async virtual Task<IActionResult> RevokeShareInvite(int shareInviteId)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await walletOperations.RevokeShareInviteAsync(shareInviteId);
                    return NoContent();
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                return HandleStandardExceptions(ex, logger);
            }
        }
        #endregion
    }
}
