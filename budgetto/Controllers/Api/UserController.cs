﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using budgetto.Common.Operations;
using budgetto.Common.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.Security.Principal;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using budgetto.Authorization;
using Microsoft.Extensions.Options;
using budgetto.Common;
using budgetto.AppCode.ExtensionMethods;
using budgetto.Common.Exceptions;

namespace budgetto.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class UserController : BaseController
    {
        protected readonly ILogger<UserController> logger;
        private readonly IJwtTokenGenerator jwtTokenGenerator;

        protected IExecutionContext executionContext;
        protected IUserOperations userOperations;

        public UserController(ILogger<UserController> logger, IJwtTokenGenerator jwtTokenGenerator, IExecutionContext executionContext, IUserOperations userOperations)
        {
            this.logger = logger;
            this.jwtTokenGenerator = jwtTokenGenerator;
            this.executionContext = executionContext;
            this.userOperations = userOperations;
        }

        /// <summary>
        /// Adds new user to system
        /// </summary>
        /// <remarks>Field `User.Id` is ignored</remarks>
        /// <param name="user">New user data</param>
        /// <response code="201">Returns the newly-created user</response>
        /// <response code="400">If the user is null or invalid</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(UserModel), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [AllowAnonymous]
        [HttpPost()]
        public async virtual Task<IActionResult> RegisterUser([FromBody] UserModel user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var registredUser = await userOperations.RegisterUserAsync(user);
                    return CreatedAtAction("GetUser", new { id = registredUser.Id }, registredUser);
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Unexpected exception");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Gets user data
        /// </summary>
        /// <param name="id">User id</param>
        /// <response code="201">Returns user from system</response>
        /// <response code="400">If the id is null or invalid</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(UserModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = AuthorizationPolicies.AuthTokenPolicy)]
        [HttpGet("{id:int}")]
        public async virtual Task<IActionResult> GetUser(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var registredUser = await userOperations.GetUserAsync<UserModel>(id);
                    return Ok(registredUser);
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                return HandleStandardExceptions(ex, logger);
            }
        }

        /// <summary>
        /// Log in user and return user and authentication data
        /// </summary>
        /// <param name="loginModel">New user data</param>
        /// <response code="200">Returns user and authentication data</response>
        /// <response code="400">If the loginModel is null or invalid</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(LoginResponseModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [AllowAnonymous]
        [HttpPost("[action]")]
        public async virtual Task<IActionResult> Login([FromBody] LoginModel loginModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var loggedUser = await userOperations.LoginUserAsync(loginModel);
                    var jwtAuthToken = jwtTokenGenerator.CreateToken(loggedUser, false, false);
                    var jwtRefreshToken = jwtTokenGenerator.CreateToken(loggedUser, true, !loginModel.DoNotLogout);

                    return Ok(new LoginResponseModel
                    {
                        AuthToken = jwtAuthToken.Token,
                        RefreshToken = jwtRefreshToken.Token,
                        AuthTokenExpirationDateTime = jwtAuthToken.ExpirationDateTime.Value,
                        User = loggedUser,
                        DoNotLogout = loginModel.DoNotLogout
                    });
                }
                else
                {
                    return BadRequest(ModelState.ToErrorsModel());
                }
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Unexpected exception");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Refresh authentication token
        /// </summary>
        /// <response code="200">Returns user and authentication data</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(LoginResponseModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorsModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = AuthorizationPolicies.RefreshTokenPolicy)]
        [HttpPost("[action]")]
        public async virtual Task<IActionResult> RefreshToken([FromBody] RefreshTokenModel refreshTokenModel)
        {
            try
            {
                var loggedUser = await userOperations.GetUserAsync<UserModel>(executionContext.GetExecutingUserId().Value);
                var jwtAuthToken = jwtTokenGenerator.CreateToken(loggedUser, false, false);
                var jwtRefreshToken = jwtTokenGenerator.CreateToken(loggedUser, true, !refreshTokenModel.DoNotLogout);

                return Ok(new LoginResponseModel
                {
                    AuthToken = jwtAuthToken.Token,
                    RefreshToken = jwtRefreshToken.Token,
                    AuthTokenExpirationDateTime = jwtAuthToken.ExpirationDateTime.Value,
                    User = loggedUser,
                    DoNotLogout = refreshTokenModel.DoNotLogout
                });
            }
            catch (BudgettoException bex)
            {
                return BadRequest(bex.Errors);
            }
            catch (UnauthorizedAccessException)
            {
                return Forbid();
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Unexpected exception");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}