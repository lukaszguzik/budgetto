﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace budgetto.Controllers.Api
{
    public class BaseController : Controller
    {
        protected virtual IActionResult HandleStandardExceptions(Exception ex, ILogger logger)
        {
            if(ex is InvalidOperationException)
            {
                return Forbid();
            }
            else
            {
                logger.LogError(ex, "Unexpected exception");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        //protected virtual int? GetLoggedUserId()
        //{
        //    int? userId = null;
        //    if (User != null && User.HasClaim(c => c.Type == ClaimTypes.NameIdentifier))
        //    {
        //        if (int.TryParse(User.FindFirst(c => c.Type == ClaimTypes.NameIdentifier).Value, out int tmpUserId))
        //        {
        //            userId = tmpUserId;
        //        }
        //    }

        //    return userId;
        //}
    }
}
