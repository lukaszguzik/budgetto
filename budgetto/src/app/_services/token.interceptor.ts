import { Injectable, Injector } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { UserService } from "./user.service";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { AuthenticationService } from "./authentication.service";
import { LoginResponseModel } from "../_models/LoginResponseModel";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  isRefreshingToken: boolean = false;
  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(private userService: UserService, private injector: Injector) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(this.addToken(request, this.isRefreshingToken))
      .catch(error => {
        if (!this.isRefreshingToken && error instanceof HttpErrorResponse) {
          switch ((<HttpErrorResponse>error).status) {
            case 401:
              return this.handle401Error(request, next, error);
            default:
              return Observable.throw(error);
          }
        } else {
          return Observable.throw(error);
        }
      });
  }

  handle401Error(request: HttpRequest<any>, next: HttpHandler, error: any) {
    if (this.userService.getLoggedUser() && !this.isRefreshingToken) {
      this.isRefreshingToken = true;
      let authService = this.injector.get(AuthenticationService);

      // Reset here so that the following requests wait until the token
      // comes back from the refreshToken call.
      this.tokenSubject.next(null);

      return authService.refreshToken()
        .switchMap((newToken: string) => {
          if (newToken) {
            this.tokenSubject.next(newToken);
            return next.handle(this.addToken(request, false));
          }

          // If we don't get a new token, we are in trouble so logout.
          this.logoutUser(authService);
          return Observable.throw(error);
        })
        .catch(error => {
          // If there is an exception calling 'refreshToken', bad news so logout.
          if(error.status == 401)
          {
            this.logoutUser(authService);
          }
          return Observable.throw(error);
        })
        .finally(() => {
          this.isRefreshingToken = false;
        });
    } else {
      return this.tokenSubject
        .filter(token => token != null)
        .take(1)
        .switchMap(token => {
          return next.handle(this.addToken(request, false));
        });
    }
  }

  addToken(request: HttpRequest<any>, refreshToken: boolean): HttpRequest<any> {
    let loggedUser = this.userService.getLoggedUser();
    if (loggedUser && loggedUser.authToken && loggedUser.refreshToken) {
      let authToken = refreshToken ? loggedUser.refreshToken : loggedUser.authToken;
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${authToken}`
        }
      });
    }

    return request;
  }

  logoutUser(authService: AuthenticationService) {
    authService.logout();
  }
}