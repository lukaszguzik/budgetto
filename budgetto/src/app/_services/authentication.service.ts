import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

import { AppConfig } from '../app.config';
import { LoginResponseModel } from "../_models/LoginResponseModel";
import { UserService } from "./user.service";
import { Observable } from "rxjs/Observable";
import { Router } from "@angular/router";

@Injectable()
export class AuthenticationService {
  constructor(private http: HttpClient, private config: AppConfig,
    private userService: UserService, private router: Router) { }

  login(email: string, password: string, doNotLogout: boolean) {
    return this.http.post(this.config.apiUrl + '/User/Login', { email: email, password: password, doNotLogout: doNotLogout })
      .map((loginResponseModel: LoginResponseModel) => {
        // login successful if there's a jwt token in the response
        if (loginResponseModel && loginResponseModel.authToken && loginResponseModel.refreshToken) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          this.userService.setLoggedUser(loginResponseModel);
        }
      });
  }

  refreshToken() : Observable<string> {
    var loggedUser = this.userService.getLoggedUser();
    return this.http.post(this.config.apiUrl + '/User/RefreshToken', { doNotLogout: loggedUser.doNotLogout })
      .map((loginResponseModel: LoginResponseModel) => {
        // login successful if there's a jwt token in the response
        if (loginResponseModel && loginResponseModel.authToken && loginResponseModel.refreshToken) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          this.userService.setLoggedUser(loginResponseModel);
        }

        return loginResponseModel.authToken;
      });
  }

  logout() {
    // remove user from local storage to log user out
    this.userService.clearLoggedUser();
    this.router.navigate(['/login']);
  }
}