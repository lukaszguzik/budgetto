import { Injectable } from "@angular/core";
import { AppConfig } from "../app.config";
import { HttpClient } from "@angular/common/http";
import { UserModel } from "../_models/UserModel";

@Injectable()
export class RegisterService {
    constructor(private http: HttpClient, private config: AppConfig) {
        
    }

    registerUser(newUser: UserModel) {
        return this.http.post(this.config.apiUrl + '/User', newUser);
    }
}