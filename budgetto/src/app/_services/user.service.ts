import { Injectable } from '@angular/core';

import { AppConfig } from '../app.config';
import { LoginResponseModel } from "../_models/LoginResponseModel";
import { LoginModel } from "../_models/LoginModel";

@Injectable()
export class UserService {
  constructor(private config: AppConfig) { }

  private static readonly loggedUserStorageKey: string = 'loggedUserData';
  private static loggedUserData: LoginResponseModel = null;
  public getLoggedUser() {
    if (!UserService.loggedUserData) {
      UserService.loggedUserData = JSON.parse(localStorage.getItem(UserService.loggedUserStorageKey));
    }

    return UserService.loggedUserData;
  }

  public setLoggedUser(loggedUser: LoginResponseModel) {
    localStorage.setItem(UserService.loggedUserStorageKey, JSON.stringify(loggedUser));
    UserService.loggedUserData = loggedUser;
  }

  public clearLoggedUser() {
    localStorage.removeItem(UserService.loggedUserStorageKey);
    UserService.loggedUserData = null;
  }
}