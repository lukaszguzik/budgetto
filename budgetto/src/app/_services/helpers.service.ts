import { ErrorsModel } from "../_models/ErrorsModel";
import { Injectable } from "@angular/core";
import { NgForm } from '@angular/forms';

@Injectable()
export class HelpersService {
    constructor() {
        
    }

    formErrorsHelper(err: any, form: NgForm) : ErrorsModel
    {
        let errorsModel = new ErrorsModel();

        if (err.status === 400) {
            // handle validation error
            errorsModel = err.error as ErrorsModel;
        } else {
        if(!errorsModel.errorMessages['other']) {
            errorsModel.errorMessages['other'] = [];
        }
        errorsModel.errorMessages['other'].push(`Unexpected error! (${err.status})`);
        }

        // handle form field errors
        for (var fieldName in errorsModel.errorMessages) {
            if (form.controls[fieldName]) {
              // integrate into angular's validation if we have field validation
              form.controls[fieldName].setErrors({ invalid: true });
          }
        }

        return errorsModel;
    }
}