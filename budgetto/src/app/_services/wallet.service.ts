import { Injectable } from '@angular/core';
// import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Http, Headers, Response } from '@angular/http';

import { AllWalletsModel } from "../_models/AllWalletsModel";
import { AppConfig } from '../app.config';
import { HttpClient } from "@angular/common/http";
import { WalletModel } from "../_models/WalletModel";

@Injectable()
export class WalletService {

    constructor(private http: HttpClient, private config: AppConfig) { }

    createWallet(newWallet: WalletModel) {
        return this.http.post(this.config.apiUrl + '/Wallet', newWallet);
    }

    getWallets(){
        return this.http.get<AllWalletsModel>(this.config.apiUrl + '/Wallet/GetAllWallets');
    }
}