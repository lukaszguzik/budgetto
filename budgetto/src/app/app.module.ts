import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
// import { NgForm } from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule, MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatTabsModule} from '@angular/material/tabs';
import {MatCheckboxModule} from '@angular/material/checkbox';

import { AppComponent } from './app.component';
import { AppConfig } from './app.config';
import { HeaderComponent } from './components/header/header.component';
import { MenuComponent } from './components/menu/menu.component';
import { AppService } from './app.service';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthGuard } from './_guards/auth.guard';
import { AuthenticationService} from './_services/authentication.service';
import {UserService} from './_services/user.service';
import { HttpModule } from "@angular/http";
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { WalletService } from "./_services/wallet.service";
import { TokenInterceptor } from "./_services/token.interceptor";
import { RegisterService } from "./_services/register.service";
import { HelpersService } from "./_services/helpers.service";
import { MyProfileComponent } from './components/my-profile/my-profile.component';
import { ExpensesComponent } from './components/expenses/expenses.component';


@NgModule({
  declarations: [AppComponent, 
    HeaderComponent, 
    MenuComponent, 
    AppComponent,
    HomeComponent,
    LoginComponent,
    DashboardComponent,
    MyProfileComponent,
    ExpensesComponent],
  imports: [ BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    // NgForm,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatTabsModule,
    MatCheckboxModule,
    MatCardModule],
  exports: [],
  providers: [HttpClientModule,
    AppService,
    AppConfig,
    AuthGuard,
    AuthenticationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    UserService,
    RegisterService,
    WalletService,
    HelpersService],
  bootstrap: [AppComponent]
})
export class AppModule {}