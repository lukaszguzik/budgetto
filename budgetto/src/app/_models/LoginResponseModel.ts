import { UserModel } from "./UserModel";

export class LoginResponseModel {
  authToken: string;
  refreshToken: string;
  authTokenExpirationDateTime: Date;
  password: string;
  email: string;
  user: UserModel;
  doNotLogout: boolean;
}