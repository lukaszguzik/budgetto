import { WalletModel } from "./WalletModel";

export class AllWalletsModel {
    ownedWallets: WalletModel[];
    sharedWallets: WalletModel[];
}