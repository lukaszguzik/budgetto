export class WalletModel {
    id: number;
    name: string;
    ownerId: number;
    ownerName: string;
}