export class ErrorsModel {
    errorMessages: { [fieldName: string]: string[] };

    constructor() {
        this.errorMessages = {};
    }
}