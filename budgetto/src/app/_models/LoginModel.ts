export class LoginModel {
  email: string;
  password: string;
  doNotLogout: boolean;
}