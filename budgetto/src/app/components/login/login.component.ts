import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

import { AuthenticationService } from '../../_services/authentication.service';
import { LoginModel } from "../../_models/LoginModel";
import { RegisterService } from "../../_services/register.service";
import { UserModel } from "../../_models/UserModel";
import { ErrorsModel } from "../../_models/ErrorsModel";
import { UserService } from "../../_services/user.service";
import { HelpersService } from "../../_services/helpers.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [String('./login.component.scss')]
})
export class LoginComponent implements OnInit {
  registerUserModel: UserModel = new UserModel();
  model: LoginModel = new LoginModel();
  returnUrl: string;
  loginErrors = new ErrorsModel();
  registerErrors = new ErrorsModel();

  constructor(private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private registerService: RegisterService,
    private userService: UserService,
    private helpersService: HelpersService) { }
  ngOnInit() {
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
    if(this.route.snapshot.queryParams['logout'] == 'true') {
      // reset login status
      this.authenticationService.logout();
    }

    if(this.userService.getLoggedUser()) {
      this.router.navigate([this.returnUrl]);
    }
  }

  login(loginForm: NgForm) {
    this.loginErrors = new ErrorsModel();
    this.authenticationService.login(this.model.email, this.model.password, this.model.doNotLogout)
      .subscribe(
      data => { 
        this.router.navigate([this.returnUrl]);
      },
      err => {
        this.loginErrors = this.helpersService.formErrorsHelper(err, loginForm);
      });
  }

  registerUser(form: NgForm) {
    this.registerService.registerUser(this.registerUserModel)
      .subscribe((registredUser: UserModel) => {
      },
      err => {
        this.registerErrors = this.helpersService.formErrorsHelper(err, form);
      });

  }
}
