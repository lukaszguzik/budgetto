import { Component, OnInit } from '@angular/core';

import { UserService } from '../../_services/user.service';
import { LoginResponseModel } from "../../_models/LoginResponseModel";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: [String('./home.component.scss')]
})
export class HomeComponent implements OnInit {

  constructor(private userService: UserService) { 
  }

  ngOnInit() {
  }
}
