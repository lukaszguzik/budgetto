import { Component, OnInit, HostListener, ElementRef } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { Router, Routes, NavigationEnd } from '@angular/router';

import { UserService } from "../../_services/user.service";
import { LoginResponseModel } from "../../_models/LoginResponseModel";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: [String('./header.component.scss')],
  animations: [
    trigger('scrollAnimation', [
      state('transparent', style({
        backgroundColor: 'transparent'
      })),
      state('colored',   style({
        backgroundColor: '#00838f',
        boxShadow: '0px 2px 4px -1px rgba(0, 0, 0, 0.2), 0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12)'
      })),
      transition('transparent => colored', animate('500ms ease-in')),
      transition('colored => transparent', animate('500ms ease-out'))
    ])
  ]
})
export class HeaderComponent implements OnInit {

  state = 'transparent';
  isHome = false;
  router: Router;
  

  constructor(private userService: UserService, private element: ElementRef, private _router: Router){
    this.router = _router;
    this.router.events.subscribe((val) => {
      if(val instanceof NavigationEnd)
      {
        this.checkScrollPositionAndRoute();
      }
    });
  }

  ngOnInit() {
  }

  @HostListener('window:scroll', ['$event'])
    checkScrollPositionAndRoute() {
      if (this.router.url === '/home') { 
        let scrollPosition = window.pageYOffset;
      
        if (scrollPosition > 0) {
          this.state = 'colored';
        } else {
          this.state = 'transparent';
        }
      } else {
        this.state = 'colored';
      }
    }


}
