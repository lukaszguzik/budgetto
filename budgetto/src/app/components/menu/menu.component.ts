import { Component, OnInit } from '@angular/core';
import { UserService } from "../../_services/user.service";
import { LoginResponseModel } from "../../_models/LoginResponseModel";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: [String('./menu.component.scss')]
})
export class MenuComponent implements OnInit {
  
  constructor(private userService: UserService) {
  }

  ngOnInit() {
  }

}
