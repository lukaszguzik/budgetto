import { Component, OnInit } from '@angular/core';

import { WalletService } from '../../_services/wallet.service';
import { WalletModel } from "../../_models/WalletModel";
import { AllWalletsModel } from "../../_models/AllWalletsModel";
import { NgForm } from "@angular/forms/src/forms";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [String('./dashboard.component.scss')]
})
export class DashboardComponent implements OnInit {

  newWallet: WalletModel = new WalletModel();
  allWallets: AllWalletsModel;
  disabledButton: boolean;
  activeWallet: string;


  constructor(private walletService: WalletService) { }

  ngOnInit() {
    this.getWallets();
  }

  getWallets(): void {
    this.walletService.getWallets()
        .subscribe(allWallets => this.allWallets = allWallets);
  }


  createWallet(newWalletForm: NgForm) {
    this.disabledButton = true;

    this.walletService.createWallet(this.newWallet)
    .subscribe(
      (newWallet: WalletModel) => {
      this.allWallets.ownedWallets.push(newWallet);
      newWalletForm.reset();
      this.disabledButton = false;
      },
      err => {
        this.disabledButton = false;
        console.log(err);
        //this.loginErrors = this.helpersService.formErrorsHelper(err, loginForm);
      }
    );
  }

  onWalletSelection() {
    console.log(this.activeWallet);
  }

  

}
