import { Component, OnInit } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [String('./app.component.scss')]
})
export class AppComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}