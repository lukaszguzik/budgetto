# Budgetto
Web application for managing home budget including expenses, incomes and saving goals.
Basic assumptions that guide the project are:
*  Layered code structure
*  Complete separation of these layers
*  Back-end code based on .Net Core 2.0
*  Front-end code based on Angular 4 (TypeScript)
*  Having fun writing code and brushing up programming skills

## Modules
#### budgetto
Main application component which includes frontend and exposes REST API for managing data.
ASP.NET Core Web API was used for delivering API and Angular 4 for the frontend.

#### budgetto.Common
.Net Core 2.0 class library containing common parts of application (i.e. interfaces or data models).

#### budgetto.BLL
Business Logic Layer wrote as .Net Core 2.0 class library containing whole application logic separated from frontend and data access layer.

#### budgetto.DAL
Data Access Layer containing managers for manipulation of data stored in MS SQL Server database. Entity Framework Core 2.0 was used with database first approach.

#### budgetto.DB
Visual Studio database project used for defining and publishing database structure.

#### UnitTests
Project containing unit tests. NUnit and Entity Framework Core 2.0 in-memory database was used.