﻿CREATE TABLE [dbo].[User]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY (1, 1), 
    [Email] NVARCHAR(254) NOT NULL, 
    [PasswordHash] VARBINARY(64) NOT NULL, 
    [PasswordSalt] VARBINARY(64) NOT NULL, 
    [Name] NVARCHAR(50) NOT NULL, 
    [HonourTokensIssuedAfter] DATETIMEOFFSET NULL
)

GO

CREATE UNIQUE INDEX [IX_User_Email] ON [dbo].[User] ([Email])
