﻿CREATE TABLE [dbo].[ShareInvite]
(
    [Id] INT NOT NULL PRIMARY KEY IDENTITY (1, 1), 
    [WalletId] INT NOT NULL, 
    [InvitedUserId] INT NOT NULL, 
    [IsReadOnly] BIT NOT NULL, 
    [Accepted] BIT NOT NULL, 
    [Active] BIT NOT NULL, 
    [AcceptToken] UNIQUEIDENTIFIER NULL, 
    [AcceptTokenValidTo] DATETIMEOFFSET NULL, 
    CONSTRAINT [FK_ShareInvite_Wallet] FOREIGN KEY ([WalletId]) REFERENCES [Wallet]([Id]), 
    CONSTRAINT [FK_ShareInvite_User] FOREIGN KEY ([InvitedUserId]) REFERENCES [User]([Id])
)

GO

CREATE UNIQUE INDEX [IX_ShareInvite_AcceptToken] ON [dbo].[ShareInvite] ([AcceptToken])
