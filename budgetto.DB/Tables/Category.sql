﻿CREATE TABLE [dbo].[Category]
(
    [Id] INT NOT NULL PRIMARY KEY IDENTITY (1, 1), 
    [WalletId] INT NOT NULL, 
    [CategoryTypeId] INT NOT NULL, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Color] NVARCHAR(6) NOT NULL, 
    CONSTRAINT [FK_Category_Wallet] FOREIGN KEY ([WalletId]) REFERENCES [Wallet]([Id]), 
    CONSTRAINT [FK_Category_CategoryType] FOREIGN KEY ([CategoryTypeId]) REFERENCES [CategoryType]([Id])
)
