﻿CREATE TABLE [dbo].[CategoryType]
(
    [Id] INT NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NOT NULL
)
