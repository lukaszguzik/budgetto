﻿CREATE TABLE [dbo].[Wallet]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY (1, 1), 
    [OwnerId] INT NOT NULL, 
    [Name] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [FK_Wallet_User] FOREIGN KEY ([OwnerId]) REFERENCES [User]([Id])
)
