﻿CREATE TABLE [dbo].[ExpenseCategory]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY (1, 1), 
    [CategoryId] INT NOT NULL, 
    [ExpenseId] INT NOT NULL, 
    CONSTRAINT [FK_ExpenseCategory_Category] FOREIGN KEY ([CategoryId]) REFERENCES [Category]([Id]),
    CONSTRAINT [FK_ExpenseCategory_Expense] FOREIGN KEY ([ExpenseId]) REFERENCES [Expense]([Id]) 
)
