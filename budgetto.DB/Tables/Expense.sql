﻿CREATE TABLE [dbo].[Expense]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY (1, 1), 
    [WalletId] INT NOT NULL,
    [Name] NVARCHAR(255) NOT NULL, 
    [Amount] MONEY NOT NULL, 
    [Date] DATETIMEOFFSET NOT NULL, 
    CONSTRAINT [FK_Expense_Wallet] FOREIGN KEY ([WalletId]) REFERENCES [Wallet]([Id]) 
)

GO

CREATE INDEX [IX_Expense_WalletId] ON [dbo].[Expense] ([WalletId])
