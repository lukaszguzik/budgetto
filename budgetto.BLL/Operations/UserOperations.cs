﻿using budgetto.Common.Operations;
using System;
using System.Collections.Generic;
using System.Text;
using budgetto.Common.Models;
using budgetto.Common.DataManagers;
using System.Security.Cryptography;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using budgetto.Common;
using budgetto.Common.Exceptions;
using budgetto.Common.Resources;

namespace budgetto.BLL.Operations
{
    public class UserOperations : BaseOperations, IUserOperations
    {
        protected IUserDataManager userDataManager;

        public UserOperations(IExecutionContext executionContext, IUserDataManager userDataManager)
            : base(executionContext)
        {
            this.userDataManager = userDataManager;
        }

        public async virtual Task<UserModel> RegisterUserAsync(UserModel newUser)
        {
            if (await userDataManager.UserExistsAsync(newUser.Email))
            {
                throw new BudgettoException(new ErrorsModel("User already registered"));
            }

            var userDetailed = new UserDetailedModel
            {
                Email = newUser.Email,
                Name = newUser.Name,
            };

            CreatePasswordHashForUser(newUser.Password, userDetailed);

            var addedUser = await userDataManager.AddUserAsync(userDetailed);

            return new UserModel
            {
                Id = addedUser.Id,
                Email = addedUser.Email,
                Name = addedUser.Name
            };
        }

        public async virtual Task<T> GetUserAsync<T>(int id) where T : UserModel
        {
            try
            {
                return await userDataManager.GetUserAsync<T>(id);
            }
            catch (InvalidOperationException)
            {
                throw new UnauthorizedAccessException();
            }
        }

        public async virtual Task<UserModel> LoginUserAsync(LoginModel loginModel)
        {
            try
            {
                var user = await userDataManager.GetUserAsync<UserDetailedModel>(loginModel.Email);
                var calculatedHash = CalculatePasswordHash(loginModel.Password, user.PasswordSalt);

                if (!calculatedHash.SequenceEqual(user.PasswordHash))
                {
                    throw new BudgettoException(new ErrorsModel(ErrorMessages.UserLoginFailed));
                }

                return new UserModel
                {
                    Id = user.Id,
                    Email = user.Email,
                    Name = user.Name
                };
            }
            catch (InvalidOperationException)
            {
                throw new BudgettoException(new ErrorsModel(ErrorMessages.UserLoginFailed));
            }
        }

        #region Private methods
        protected virtual void CreatePasswordHashForUser(string password, UserDetailedModel userDetailed)
        {
            var passwordBytes = Encoding.UTF8.GetBytes(password);
            userDetailed.PasswordSalt = new byte[ModelsConstValues.USER_SALT_SIZE];
            var rnd = new Random();
            rnd.NextBytes(userDetailed.PasswordSalt);

            var sha = SHA512.Create();
            sha.TransformBlock(passwordBytes, 0, passwordBytes.Length, passwordBytes, 0);
            sha.TransformFinalBlock(userDetailed.PasswordSalt, 0, userDetailed.PasswordSalt.Length);

            userDetailed.PasswordHash = sha.Hash;
        }

        protected virtual byte[] CalculatePasswordHash(string password, byte[] salt)
        {
            var passwordBytes = Encoding.UTF8.GetBytes(password);

            var sha = SHA512.Create();
            sha.TransformBlock(passwordBytes, 0, passwordBytes.Length, passwordBytes, 0);
            sha.TransformFinalBlock(salt, 0, salt.Length);

            return sha.Hash;
        }
        #endregion
    }
}
