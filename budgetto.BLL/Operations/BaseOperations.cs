﻿using budgetto.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace budgetto.BLL.Operations
{
    public class BaseOperations
    {
        protected IExecutionContext executionContext;
        public BaseOperations(IExecutionContext executionContext)
        {
            this.executionContext = executionContext;
        }
    }
}
