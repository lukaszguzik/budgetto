﻿using budgetto.Common;
using budgetto.Common.DataManagers;
using budgetto.Common.Exceptions;
using budgetto.Common.Models;
using budgetto.Common.Operations;
using budgetto.Common.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace budgetto.BLL.Operations
{
    public class WalletOperations : BaseOperations, IWalletOperations
    {
        protected IWalletDataManager walletDataManager;
        protected IUserDataManager userDataManager;
        protected ICategoryDataManager categoryDataManager;
        protected IExpenseDataManager expenseDataManager;

        public WalletOperations(IExecutionContext executionContext, IWalletDataManager walletDataManager, IUserDataManager userDataManager,
            ICategoryDataManager categoryDataManager, IExpenseDataManager expenseDataManager)
            : base (executionContext)
        {
            this.walletDataManager = walletDataManager;
            this.userDataManager = userDataManager;
            this.categoryDataManager = categoryDataManager;
            this.expenseDataManager = expenseDataManager;
        }

        #region Wallet operations
        public async virtual Task<WalletModel> CreateWalletAsync(WalletModel newWallet)
        {
            newWallet.OwnerId = executionContext.GetExecutingUserId().Value;
            newWallet = await walletDataManager.AddWalletAsync(newWallet);
            newWallet.OwnerName = executionContext.GetExecutingUserName();

            return newWallet;
        }

        public async virtual Task<WalletModel> GetWalletAsync(int walletId)
        {
            var wallet = await walletDataManager.GetWalletAsync(walletId);
            await CheckWalletExistenceAndRightAsync(wallet, false);

            return wallet;
        }

        public async Task<IEnumerable<WalletModel>> GetAllOwnedWalletsAsync()
        {
            return await walletDataManager.GetAllOwnedWalletsAsync(executionContext.GetExecutingUserId().Value);
        }

        public async Task<IEnumerable<WalletModel>> GetAllSharedWalletsAsync()
        {
            return await walletDataManager.GetAllSharedWalletsAsync(executionContext.GetExecutingUserId().Value);
        }

        public async Task<AllWalletsModel> GetAllWalletsAsync()
        {
            return await walletDataManager.GetAllWalletsAsync(executionContext.GetExecutingUserId().Value);
        }
        #endregion

        #region Category operations
        public async virtual Task<CategoryModel> CreateCategoryAsync(int walletId, CategoryModel category, CategoryTypeModel type)
        {
            var wallet = await walletDataManager.GetWalletAsync(walletId);
            await CheckWalletExistenceAndRightAsync(wallet, true);

            return await categoryDataManager.AddCategoryAsync(walletId, category, type);
        }

        public async virtual Task<CategoryModel> GetCategoryAsync(int walletId, int id, CategoryTypeModel type)
        {
            var wallet = await walletDataManager.GetWalletAsync(walletId);
            await CheckWalletExistenceAndRightAsync(wallet, false);

            return await categoryDataManager.GetCategoryAsync(walletId, id, type);
        }

        public async virtual Task<IEnumerable<CategoryModel>> GetAllCategoriesAsync(int walletId, CategoryTypeModel type)
        {
            var wallet = await walletDataManager.GetWalletAsync(walletId);
            await CheckWalletExistenceAndRightAsync(wallet, false);

            return await categoryDataManager.GetAllCategoriesAsync(walletId, type);
        }

        public async virtual Task EditCategoryAsync(int walletId, int id, CategoryModel category, CategoryTypeModel type)
        {
            var wallet = await walletDataManager.GetWalletAsync(walletId);
            await CheckWalletExistenceAndRightAsync(wallet, true);

            await categoryDataManager.UpdateCategoryAsync(walletId, id, category, type);
        }
        #endregion

        #region Expense operations
        public async virtual Task<ExpenseModel> CreateExpenseAsync(int walletId, ExpenseModel expense)
        {
            var wallet = await walletDataManager.GetWalletAsync(walletId);
            await CheckWalletExistenceAndRightAsync(wallet, true);

            return await expenseDataManager.AddExpenseAsync(walletId, expense);
        }

        public async virtual Task<ExpenseModel> GetExpenseAsync(int walletId, int id)
        {
            var wallet = await walletDataManager.GetWalletAsync(walletId);
            await CheckWalletExistenceAndRightAsync(wallet, false);

            return await expenseDataManager.GetExpenseAsync(walletId, id);
        }

        public async virtual Task<ExpensesModel> GetExpensesAsync(int walletId,
            int pageNo, int countOnPage,
            OrderTypeModel orderType,
            string name = null,
            decimal? minAmount = null, decimal? maxAmount = null,
            DateTimeOffset? minDate = null, DateTimeOffset? maxDate = null,
            List<int> categoryIds = null)
        {
            var wallet = await walletDataManager.GetWalletAsync(walletId);
            await CheckWalletExistenceAndRightAsync(wallet, false);

            return await expenseDataManager.GetExpensesAsync(walletId, pageNo, countOnPage, orderType, name, minAmount, maxAmount, minDate, maxDate, categoryIds);
        }

        public async virtual Task UpdateExpenseAsync(int walletId, int id, ExpenseModel expense)
        {
            var wallet = await walletDataManager.GetWalletAsync(walletId);
            await CheckWalletExistenceAndRightAsync(wallet, true);

            await expenseDataManager.UpdateExpenseAsync(walletId, id, expense);
        }
        #endregion

        #region ShareInvite operations
        public async Task<SendShareInviteModel> SendShareInviteAsync(int walletId, SendShareInviteModel sendShareInviteModel)
        {
            if(sendShareInviteModel.InviteValidTo < DateTimeOffset.Now)
            {
                throw new BudgettoException(new ErrorsModel(ErrorMessages.InviteValidToDateInPast));
            }

            var wallet = await walletDataManager.GetWalletAsync(walletId);
            if (wallet.OwnerId != executionContext.GetExecutingUserId().Value)
            {
                throw new UnauthorizedAccessException();
            }

            UserModel invitedUser;

            try
            {
                invitedUser = await userDataManager.GetUserAsync<UserModel>(sendShareInviteModel.UserEmail);
            }
            catch (InvalidOperationException)
            {
                throw new BudgettoException(new ErrorsModel(ErrorMessages.UserNotFound));
            }

            if(invitedUser.Id == executionContext.GetExecutingUserId())
            {
                throw new BudgettoException(new ErrorsModel(ErrorMessages.CannotSendInviteToSelf));
            }

            var walletShareInvites = await walletDataManager.GetAllShareInvitesForWalletAsync(walletId);
            if(walletShareInvites.Any(
                wsi => wsi.InvitedUserId == invitedUser.Id
                && wsi.WalletId == walletId
                && wsi.Active
                && (wsi.AcceptTokenValidTo > DateTimeOffset.Now || wsi.Accepted)))
            {
                throw new BudgettoException(new ErrorsModel(ErrorMessages.InviteAlreadySent));
            }

            var shareInviteModel = new ShareInviteModel
            {
                IsReadOnly = sendShareInviteModel.IsReadOnly,
                InvitedUserId = invitedUser.Id,
                Accepted = false,
                Active = true,
                AcceptToken = Guid.NewGuid(),
                AcceptTokenValidTo = sendShareInviteModel.InviteValidTo
            };

            shareInviteModel = await walletDataManager.AddShareInviteAsync(walletId, shareInviteModel);

            return sendShareInviteModel;
        }

        public async Task<IEnumerable<ShareInviteModel>> GetShareInvitesForWalletAsync(int walletId)
        {
            var wallet = await walletDataManager.GetWalletAsync(walletId);
            if (wallet.OwnerId != executionContext.GetExecutingUserId().Value)
            {
                throw new UnauthorizedAccessException();
            }

            return await walletDataManager.GetAllShareInvitesForWalletAsync(walletId);
        }

        public async Task<IEnumerable<ShareInviteModel>> GetShareInvitesForUserAsync()
        {
            var shareInvites = await walletDataManager.GetAllShareInvitesForUserAsync(executionContext.GetExecutingUserId().Value);
            return shareInvites.Where(si => si.Active);
        }

        public async Task AcceptShareInviteAsync(Guid acceptToken)
        {
            var shareInvite = await walletDataManager.GetShareInviteAsync(acceptToken);
            if(shareInvite.InvitedUserId != executionContext.GetExecutingUserId()
                || shareInvite.AcceptTokenValidTo < DateTimeOffset.Now)
            {
                throw new UnauthorizedAccessException();
            }

            shareInvite.Accepted = true;
            await walletDataManager.UpdateShareInviteAsync(shareInvite.Id, shareInvite);

        }

        public async Task RevokeShareInviteAsync(int shareInviteId)
        {
            var shareInvite = await walletDataManager.GetShareInviteAsync(shareInviteId);
            var wallet = await walletDataManager.GetWalletAsync(shareInvite.WalletId);
            if(shareInvite.InvitedUserId != executionContext.GetExecutingUserId()
                && wallet.OwnerId != executionContext.GetExecutingUserId())
            {
                    throw new UnauthorizedAccessException();
            }

            if (executionContext.GetExecutingUserId() == wallet.OwnerId)
            {
                shareInvite.Active = false;
            }
            else
            {
                shareInvite.Accepted = false;
            }

            await walletDataManager.UpdateShareInviteAsync(shareInvite.Id, shareInvite);
        }
        #endregion

        #region Private methods
        private async Task CheckWalletExistenceAndRightAsync(WalletModel wallet, bool notReadOnlyRequired)
        {
            if (wallet.OwnerId != executionContext.GetExecutingUserId().Value)
            {
                try
                {
                    var shareInvite = await walletDataManager.GetShareInviteForUserAndWalletAsync(executionContext.GetExecutingUserId().Value, wallet.Id);
                    if (!shareInvite.Accepted || !shareInvite.Active || (shareInvite.IsReadOnly && notReadOnlyRequired))
                    {
                        throw new UnauthorizedAccessException();
                    }
                }
                catch(InvalidOperationException) //there is no share invite for the wallet
                {
                    throw new UnauthorizedAccessException();
                }
            }
        }
        #endregion
    }
}
