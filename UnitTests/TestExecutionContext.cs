﻿using budgetto.Common;
using budgetto.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTests
{
    class TestExecutionContext : IExecutionContext
    {
        private int? userId;
        private string userName;

        public TestExecutionContext(int? userId, BudgettoContext dbContext)
        {
            this.userId = userId;
            userName = null;
            if(userId != null && dbContext != null)
            {
                var usr = dbContext.User.FirstOrDefault(u => u.Id == userId);
                userName = usr?.Name;
            }
        }

        public int? GetExecutingUserId()
        {
            return userId;
        }

        public string GetExecutingUserName()
        {
            return userName;
        }
    }
}
