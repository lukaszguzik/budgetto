﻿using budgetto.Common.Models;
using budgetto.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTests
{
    class TestData
    {
        public BudgettoContext DbContext { get; set; }

        public List<int> UserIds { get; set; }
        public List<int> WalletIds { get; set; }
        public List<int> WalletsWithShareInvitesIds { get; set; }

        public TestData(BudgettoContext dbContext)
        {
            this.DbContext = dbContext;

            AddUsers();
            AddWallets();
            AddCategoryTypes();
            AddCategories();
            AddExpenses();
            AddShareInvites();
        }

        private void AddUsers()
        {
            for (int i = 0; i < 5; i++)
            {
                DbContext.User.Add(new User
                {
                    Email = $"test{i}@test.pl",
                    PasswordHash = HelperMethods.HexStringToByteArray("314812FEBA22557AAD4AA2ABD20A817C7FAE8C4A761F737A18061BBFDEDAAEDF63264CF0F0AD731446E5074684C30F1A69B5317065C210AEC9BE6323B50165E2"),
                    PasswordSalt = HelperMethods.HexStringToByteArray("E74B8C05BAF884A8F3AB951D3C7164F4C43A714ED344E97DE27C4150BA81CF0F3B46F6C70B92CEF016BB45D79FFA003B38FACC734BC342A2620E223DB634EE72"),
                    Name = $"User{i}"
                });
            }

            DbContext.SaveChanges();

            UserIds = DbContext.User.Select(u => u.Id).ToList();
        }

        private void AddWallets()
        {
            foreach (var usr in DbContext.User)
            {
                for (int i = 0; i < 5; i++)
                {
                    DbContext.Wallet.Add(new Wallet
                    {
                        Name = $"User{usr.Id}_Wallet{i}",
                        OwnerId = usr.Id
                    });
                }
            }

            DbContext.SaveChanges();

            WalletIds = DbContext.Wallet.Select(w => w.Id).ToList();
        }

        private void AddCategoryTypes()
        {
            DbContext.CategoryType.AddRange(new CategoryType[]
                {
                    new CategoryType
                    {
                        Id = (int)CategoryTypeModel.ExpenseCategory,
                        Name = "ExpenseCategory"
                    },
                    new CategoryType
                    {
                        Id = (int)CategoryTypeModel.IncomeCategory,
                        Name = "IncomeCategory"
                    }
                });

            DbContext.SaveChanges();
        }

        private void AddCategories()
        {
            foreach (var wallet in DbContext.Wallet)
            {
                for (int i = 0; i < 3; i++)
                {
                    DbContext.Category.Add(new Category
                    {
                        WalletId = wallet.Id,
                        Name = $"Wallet{wallet.Id}_ExpCategory{i}",
                        CategoryTypeId = (int)CategoryTypeModel.ExpenseCategory,
                        Color = "000000"
                    });

                    DbContext.Category.Add(new Category
                    {
                        WalletId = wallet.Id,
                        Name = $"Wallet{wallet.Id}_IncCategory{i}",
                        CategoryTypeId = (int)CategoryTypeModel.IncomeCategory,
                        Color = "000000"
                    });
                }
            }

            DbContext.SaveChanges();
        }

        private void AddExpenses()
        {
            var rnd = new Random();
            foreach (var wallet in DbContext.Wallet)
            {
                for(int i = 0; i < 5; i++)
                {
                    var categories = wallet.Category.Where(c => c.CategoryTypeId == (int)CategoryTypeModel.ExpenseCategory).ToList();

                    var exp = DbContext.Expense.Add(new Expense
                    {
                        WalletId = wallet.Id,
                        Name = $"Wallet{wallet.Id}_Expense{i}",
                        Amount = (decimal)(rnd.NextDouble() * 100.00),
                        Date = DateTimeOffset.Now.Subtract(new TimeSpan(rnd.Next(30), rnd.Next(24), rnd.Next(60), rnd.Next(60)))
                    });

                    for(int j = 0; j < rnd.Next(categories.Count); j++)
                    {
                        exp.Entity.ExpenseCategory.Add(new ExpenseCategory
                        {
                            CategoryId = categories[j].Id,
                        });
                    }
                }
            }

            DbContext.SaveChanges();
        }

        private void AddShareInvites()
        {
            WalletsWithShareInvitesIds = DbContext.Wallet.Where(w => w.OwnerId == UserIds[0])
                .OrderByDescending(w => w.Id)
                .Select(w => w.Id)
                .Take(2)
                .ToList();

            DbContext.ShareInvite.Add(new ShareInvite
            {
                WalletId = WalletsWithShareInvitesIds[0],
                InvitedUserId = UserIds[1],
                IsReadOnly = false,
                Accepted = false,
                Active = true,
                AcceptToken = Guid.NewGuid(),
                AcceptTokenValidTo = DateTimeOffset.Now.AddDays(10)
            });

            DbContext.ShareInvite.Add(new ShareInvite
            {
                WalletId = WalletsWithShareInvitesIds[0],
                InvitedUserId = UserIds[2],
                IsReadOnly = true,
                Accepted = true,
                Active = true,
                AcceptToken = Guid.NewGuid(),
                AcceptTokenValidTo = DateTimeOffset.Now.AddDays(10)
            });

            DbContext.ShareInvite.Add(new ShareInvite
            {
                WalletId = WalletsWithShareInvitesIds[0],
                InvitedUserId = UserIds[3],
                IsReadOnly = true,
                Accepted = true,
                Active = false,
                AcceptToken = Guid.NewGuid(),
                AcceptTokenValidTo = DateTimeOffset.Now.AddDays(10)
            });

            DbContext.ShareInvite.Add(new ShareInvite
            {
                WalletId = WalletsWithShareInvitesIds[1],
                InvitedUserId = UserIds[2],
                IsReadOnly = false,
                Accepted = true,
                Active = true,
                AcceptToken = Guid.NewGuid(),
                AcceptTokenValidTo = DateTimeOffset.Now.AddDays(-1)
            });

            DbContext.ShareInvite.Add(new ShareInvite
            {
                WalletId = WalletsWithShareInvitesIds[1],
                InvitedUserId = UserIds[3],
                IsReadOnly = false,
                Accepted = true,
                Active = true,
                AcceptToken = Guid.NewGuid(),
                AcceptTokenValidTo = DateTimeOffset.Now.AddDays(-1)
            });

            DbContext.SaveChanges();
        }
    }
}
