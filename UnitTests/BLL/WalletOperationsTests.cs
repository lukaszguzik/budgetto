﻿using budgetto.BLL.Operations;
using budgetto.Common;
using budgetto.Common.Exceptions;
using budgetto.Common.Models;
using budgetto.Common.Operations;
using budgetto.Common.Resources;
using budgetto.DAL.DataManagers;
using budgetto.DAL.Models;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.BLL
{
    [TestFixture]
    [TestOf(typeof(WalletOperations))]
    class WalletOperationsTests
    {
        BudgettoContext dbContext;
        TestData testData;

        [SetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<BudgettoContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            dbContext = new BudgettoContext(options);

            testData = new TestData(dbContext);
        }

        #region Wallet operations
        [Test]
        public async Task CreateWalletOK()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var wallet = await walletOps.CreateWalletAsync(new WalletModel
            {
                Name = "created wallet"
            });


            Assert.AreEqual("created wallet", wallet.Name);
            Assert.AreEqual(testData.UserIds[0], wallet.OwnerId);
            Assert.AreEqual("User0", wallet.OwnerName);
        }

        [Test]
        public async Task GetWalletOK()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var walletToTest = dbContext.Wallet.Include(w => w.Owner).First(w => w.OwnerId == testData.UserIds[0]);

            var wallet = await walletOps.GetWalletAsync(walletToTest.Id);

            Assert.AreEqual(walletToTest.Id, wallet.Id);
            Assert.AreEqual(walletToTest.Name, wallet.Name);
            Assert.AreEqual(walletToTest.OwnerId, wallet.OwnerId);
            Assert.AreEqual(walletToTest.Owner.Name, wallet.OwnerName);
        }

        [Test]
        public void GetWalletOtherUser()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var walletToTest = dbContext.Wallet.First(w => w.OwnerId == testData.UserIds[1]);

            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await walletOps.GetWalletAsync(walletToTest.Id));
        }

        [Test]
        public void GetWalletNonExistent()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));

            Assert.ThrowsAsync<InvalidOperationException>(async () => await walletOps.GetWalletAsync(int.MaxValue));
        }

        [Test]
        public async Task GetWalletAllOwnedWalletsOK()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));

            var allOwnedWallets = await walletOps.GetAllOwnedWalletsAsync();

            Assert.AreEqual(5, allOwnedWallets.Count());
            Assert.That(allOwnedWallets.Select(w => w.OwnerId), Is.All.EqualTo(testData.UserIds[0]));
        }

        [Test]
        public async Task GetWalletAllSharedWalletsOK()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[2], dbContext));

            var allSharedWallets = await walletOps.GetAllSharedWalletsAsync();

            Assert.AreEqual(2, allSharedWallets.Count());
            Assert.That(allSharedWallets.Select(w => w.OwnerId), Is.All.Not.EqualTo(testData.UserIds[2]));
        }

        [Test]
        public async Task GetWalletAllWalletsOK()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));

            var allWallets = await walletOps.GetAllWalletsAsync();

            Assert.AreEqual(5, allWallets.OwnedWallets.Count());
            Assert.That(allWallets.OwnedWallets.Select(w => w.OwnerId), Is.All.EqualTo(testData.UserIds[0]));
        }
        #endregion

        #region Category operations
        public async Task CreateCategoryOK()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var walletToTest = dbContext.Wallet.First(w => w.OwnerId == testData.UserIds[0]);

            var category = await walletOps.CreateCategoryAsync(walletToTest.Id, new CategoryModel
            {
                Name = "created category",
                Color = "FFFF00"
            },
            CategoryTypeModel.ExpenseCategory);


            Assert.AreEqual("created category", category.Name);
            Assert.AreEqual("FFFF00", category.Color);
        }

        public void CreateCategoryOtherUsersWallet()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var walletToTest = dbContext.Wallet.First(w => w.OwnerId == testData.UserIds[1]);

            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await walletOps.CreateCategoryAsync(walletToTest.Id, new CategoryModel
            {
                Name = "created category",
                Color = "FFFF00"
            },
            CategoryTypeModel.ExpenseCategory));
        }

        public async Task GetGategoryOK()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var categoryToTest = dbContext.Category.Include(c => c.Wallet).First(c => c.Wallet.OwnerId == testData.UserIds[0]);

            var category = await walletOps.GetCategoryAsync(categoryToTest.WalletId, categoryToTest.Id, (CategoryTypeModel)categoryToTest.CategoryTypeId);

            Assert.AreEqual(categoryToTest.Id, category.Id);
            Assert.AreEqual(categoryToTest.Name, category.Name);
            Assert.AreEqual(categoryToTest.Color, category.Color);
        }

        public void GetGategoryOtherUsersWallet()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var categoryToTest = dbContext.Category.Include(c => c.Wallet).First(c => c.Wallet.OwnerId == testData.UserIds[1]);

            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await walletOps.GetCategoryAsync(categoryToTest.WalletId, categoryToTest.Id, (CategoryTypeModel)categoryToTest.CategoryTypeId));
        }

        public void GetGategoryNonExistent()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var categoryToTest = dbContext.Category.Include(c => c.Wallet).First(c => c.Wallet.OwnerId == testData.UserIds[0]);

            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await walletOps.GetCategoryAsync(categoryToTest.WalletId, int.MaxValue, (CategoryTypeModel)categoryToTest.CategoryTypeId));
        }

        public void GetGategoryNonExistentWallet()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var categoryToTest = dbContext.Category.Include(c => c.Wallet).First(c => c.Wallet.OwnerId == testData.UserIds[0]);

            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await walletOps.GetCategoryAsync(int.MaxValue, categoryToTest.Id, (CategoryTypeModel)categoryToTest.CategoryTypeId));
        }

        public async Task EditCategoryOk()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var categoryToTest = dbContext.Category.Include(c => c.Wallet).First(c => c.Wallet.OwnerId == testData.UserIds[0]);
            var actualColor = categoryToTest.Color;

            await walletOps.EditCategoryAsync(categoryToTest.WalletId, categoryToTest.Id, new CategoryModel
            {
                Name = "edited category"
            },
            (CategoryTypeModel)categoryToTest.CategoryTypeId);

            Assert.AreEqual("edited category", categoryToTest.Name);
            Assert.AreEqual(actualColor, categoryToTest.Color);
        }

        public void EditCategoryOtherUsersWallet()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[1], dbContext));
            var categoryToTest = dbContext.Category.Include(c => c.Wallet).First(c => c.Wallet.OwnerId == testData.UserIds[0]);
            var actualName = categoryToTest.Name;
            var actualColor = categoryToTest.Color;

            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await walletOps.EditCategoryAsync(categoryToTest.WalletId, categoryToTest.Id, new CategoryModel
            {
                Name = "edited category",
                Color = "FFAABB"
            },
            (CategoryTypeModel)categoryToTest.CategoryTypeId));

            Assert.AreEqual(actualName, categoryToTest.Name);
            Assert.AreEqual(actualColor, categoryToTest.Color);
        }
        #endregion

        #region Expense operations
        [Test]
        public async Task GetExpenseOK()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var expenseToTest = dbContext.Expense.Include(e => e.Wallet).Include(e => e.ExpenseCategory)
                .First(e => e.Wallet.OwnerId == testData.UserIds[0]);
            var categories = expenseToTest.ExpenseCategory.Select(ec => ec.CategoryId).OrderBy(cid => cid).ToList();

            var expense = await walletOps.GetExpenseAsync(expenseToTest.WalletId, expenseToTest.Id);

            Assert.AreEqual(expenseToTest.Name, expense.Name);
            Assert.AreEqual(expenseToTest.Amount, expense.Amount);
            Assert.AreEqual(expenseToTest.Date, expense.Date);
            Assert.That(expense.CategoryIds.OrderBy(cid => cid), Is.EqualTo(categories));
        }

        [Test]
        public void GetExpenseNonExistent()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var expenseToTest = dbContext.Expense.Include(e => e.Wallet).Include(e => e.ExpenseCategory)
                .First(e => e.Wallet.OwnerId == testData.UserIds[0]);

            Assert.ThrowsAsync<InvalidOperationException>(async () => await walletOps.GetExpenseAsync(expenseToTest.WalletId, int.MaxValue));
        }

        [Test]
        public void GetExpenseNonExistentWallet()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var expenseToTest = dbContext.Expense.Include(e => e.Wallet).Include(e => e.ExpenseCategory)
                .First(e => e.Wallet.OwnerId == testData.UserIds[0]);

            Assert.ThrowsAsync<InvalidOperationException>(async () => await walletOps.GetExpenseAsync(int.MaxValue, expenseToTest.Id));
        }

        [Test]
        public async Task GetExpensesOK()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var walletId = dbContext.Wallet.Where(w => w.OwnerId == testData.UserIds[0]).Select(w => w.Id).First();

            var expenses = await walletOps.GetExpensesAsync(walletId, 1, 3, OrderTypeModel.NameAscending);

            Assert.AreEqual(3, expenses.Expenses.Count());
            Assert.AreEqual(5, expenses.Total);
            Assert.AreEqual(2, expenses.PageCount);
        }

        [Test]
        public void GetExpensesOtherUsersWallet()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var walletId = dbContext.Wallet.Where(w => w.OwnerId == testData.UserIds[1]).Select(w => w.Id).First();

            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await walletOps.GetExpensesAsync(walletId, 0, 100, OrderTypeModel.AmountAscending));
        }

        [Test]
        public void GetExpensesNonExistentWallet()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));

            Assert.ThrowsAsync<InvalidOperationException>(async () => await walletOps.GetExpensesAsync(int.MaxValue, 0, 100, OrderTypeModel.AmountAscending));
        }
        #endregion

        #region ShareInvite operations
        [Test]
        public async Task SendShareInviteOK()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var walletId = dbContext.Wallet.Where(w => w.OwnerId == testData.UserIds[0]).Select(w => w.Id).First();

            var shareInvite = await walletOps.SendShareInviteAsync(walletId, new SendShareInviteModel
            {
                UserEmail = "test1@test.pl",
                IsReadOnly = false,
                InviteValidTo = DateTimeOffset.Now.AddDays(1)
            });

            Assert.AreEqual("test1@test.pl", shareInvite.UserEmail);
            Assert.IsFalse(shareInvite.IsReadOnly);
            Assert.Greater(shareInvite.InviteValidTo, DateTimeOffset.Now);

        }

        [Test]
        public void SendShareInviteValidToPast()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var walletId = dbContext.Wallet.Where(w => w.OwnerId == testData.UserIds[1]).Select(w => w.Id).First();

            var ex = Assert.ThrowsAsync<BudgettoException>(async () => await walletOps.SendShareInviteAsync(walletId, new SendShareInviteModel
            {
                UserEmail = "test1@test.pl",
                IsReadOnly = false,
                InviteValidTo = DateTimeOffset.Now.AddDays(-1)
            }));

            Assert.AreEqual(ErrorMessages.InviteValidToDateInPast, ex.Errors.ErrorMessages[CommonConstValues.KeyForErrorsWithoutField].First());
        }

        [Test]
        public void SendShareInviteToNonExistentUser()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var walletId = dbContext.Wallet.Where(w => w.OwnerId == testData.UserIds[0]).Select(w => w.Id).First();

            var ex = Assert.ThrowsAsync<BudgettoException>(async () => await walletOps.SendShareInviteAsync(walletId, new SendShareInviteModel
            {
                UserEmail = "nonexistent@test.pl",
                IsReadOnly = false,
                InviteValidTo = DateTimeOffset.Now.AddDays(1)
            }));

            Assert.AreEqual(ErrorMessages.UserNotFound, ex.Errors.ErrorMessages[CommonConstValues.KeyForErrorsWithoutField].First());
        }

        [Test]
        public void SendShareInviteToSelf()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var walletId = dbContext.Wallet.Where(w => w.OwnerId == testData.UserIds[0]).Select(w => w.Id).First();

            var ex = Assert.ThrowsAsync<BudgettoException>(async () => await walletOps.SendShareInviteAsync(walletId, new SendShareInviteModel
            {
                UserEmail = "test0@test.pl",
                IsReadOnly = false,
                InviteValidTo = DateTimeOffset.Now.AddDays(1)
            }));

            Assert.AreEqual(ErrorMessages.CannotSendInviteToSelf, ex.Errors.ErrorMessages[CommonConstValues.KeyForErrorsWithoutField].First());
        }

        [Test]
        public void SendShareInviteAlreadySent()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var shareInvite = dbContext.ShareInvite
                .Where(si => si.InvitedUserId == testData.UserIds[3]
                && si.Accepted && si.Active).First();

            var ex = Assert.ThrowsAsync<BudgettoException>(async () => await walletOps.SendShareInviteAsync(shareInvite.WalletId, new SendShareInviteModel
            {
                UserEmail = "test3@test.pl",
                IsReadOnly = false,
                InviteValidTo = DateTimeOffset.Now.AddDays(1)
            }));

            Assert.AreEqual(ErrorMessages.InviteAlreadySent, ex.Errors.ErrorMessages[CommonConstValues.KeyForErrorsWithoutField].First());
        }

        [Test]
        public async Task GetShareInvitesForWalletOK()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));

            var shareInvites = await walletOps.GetShareInvitesForWalletAsync(testData.WalletsWithShareInvitesIds[0]);

            Assert.AreEqual(3, shareInvites.Count());
            Assert.That(shareInvites.Select(si => si.WalletId), Is.All.EqualTo(testData.WalletsWithShareInvitesIds[0]));

        }

        [Test]
        public void GetShareInvitesForWalletOtherUsersWallet()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var walletId = dbContext.Wallet.Where(w => w.OwnerId == testData.UserIds[1]).Select(w => w.Id).First();

            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await walletOps.GetShareInvitesForWalletAsync(walletId));
        }

        [Test]
        public async Task GetShareInvitesForUser()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[2], dbContext));

            var shareInvites = await walletOps.GetShareInvitesForUserAsync();

            Assert.AreEqual(2, shareInvites.Count());
            Assert.That(shareInvites.Select(si => si.InvitedUserId), Is.All.EqualTo(testData.UserIds[2]));

        }

        [Test]
        public async Task AcceptShareInviteOK()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[1], dbContext));
            var shareInvite = dbContext.ShareInvite.First(
                si => si.Accepted == false
                && si.Active == true
                && si.AcceptTokenValidTo > DateTimeOffset.Now
                && si.InvitedUserId == testData.UserIds[1]);

            await walletOps.AcceptShareInviteAsync(shareInvite.AcceptToken.Value);

            Assert.IsTrue(shareInvite.Accepted);

        }

        [Test]
        public void AcceptShareInviteOtherUser()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var shareInvite = dbContext.ShareInvite.First(
                si => si.Accepted == false
                && si.Active == true
                && si.AcceptTokenValidTo > DateTimeOffset.Now
                && si.InvitedUserId == testData.UserIds[1]);

            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await walletOps.AcceptShareInviteAsync(shareInvite.AcceptToken.Value));

            Assert.IsFalse(shareInvite.Accepted);

        }

        [Test]
        public async Task RevokeShareInviteOKInvited()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[2], dbContext));
            var shareInvite = dbContext.ShareInvite.First(
                si => si.Accepted == true
                && si.Active == true
                && si.InvitedUserId == testData.UserIds[2]);

            await walletOps.RevokeShareInviteAsync(shareInvite.Id);

            Assert.IsFalse(shareInvite.Accepted);
            Assert.IsTrue(shareInvite.Active);

        }

        [Test]
        public async Task RevokeShareInviteOKInviting()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[0], dbContext));
            var shareInvite = dbContext.ShareInvite.First(
                si => si.Accepted == true
                && si.Active == true
                && si.InvitedUserId == testData.UserIds[2]);

            await walletOps.RevokeShareInviteAsync(shareInvite.Id);

            Assert.IsTrue(shareInvite.Accepted);
            Assert.IsFalse(shareInvite.Active);

        }

        [Test]
        public void RevokeShareInviteOtherUser()
        {
            var walletOps = InstantiateWalletOps(new TestExecutionContext(testData.UserIds[1], dbContext));
            var shareInvite = dbContext.ShareInvite.First(
                si => si.Accepted == true
                && si.Active == true
                && si.InvitedUserId == testData.UserIds[2]);

            Assert.ThrowsAsync<UnauthorizedAccessException>(async () => await walletOps.RevokeShareInviteAsync(shareInvite.Id));

            Assert.IsTrue(shareInvite.Accepted);
            Assert.IsTrue(shareInvite.Active);

        }
        #endregion

        private IWalletOperations InstantiateWalletOps(IExecutionContext executionContext)
        {
            return new WalletOperations(executionContext,
                new WalletDataManager(dbContext),
                new UserDataManager(dbContext),
                new CategoryDataManager(dbContext),
                new ExpenseDataManager(dbContext));
        }
    }
}
