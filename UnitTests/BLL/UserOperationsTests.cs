﻿using budgetto.BLL.Operations;
using budgetto.Common;
using budgetto.Common.Exceptions;
using budgetto.Common.Models;
using budgetto.Common.Resources;
using budgetto.DAL.DataManagers;
using budgetto.DAL.Models;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.BLL
{
    [TestFixture]
    [TestOf(typeof(UserOperations))]
    class UserOperationsTests
    {
        BudgettoContext dbContext;
        TestData testData;

        [SetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<BudgettoContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            dbContext = new BudgettoContext(options);

            testData = new TestData(dbContext);
        }

        [Test]
        public async Task GetUserModel()
        {
            var userOps = new UserOperations(new TestExecutionContext(testData.UserIds[0], dbContext), new UserDataManager(dbContext));
            var usr = await userOps.GetUserAsync<UserModel>(testData.UserIds[0]);

            Assert.AreEqual("test0@test.pl", usr.Email);
            Assert.AreEqual("User0", usr.Name);
            Assert.IsNull(usr.Password);
        }

        [Test]
        public async Task GetUserDetailedModel()
        {
            var userOps = new UserOperations(new TestExecutionContext(testData.UserIds[1], dbContext), new UserDataManager(dbContext));
            var usr = await userOps.GetUserAsync<UserDetailedModel>(testData.UserIds[1]);

            Assert.AreEqual("test1@test.pl", usr.Email);
            Assert.AreEqual("User1", usr.Name);
            Assert.IsNull(usr.Password);
            Assert.AreEqual(HelperMethods.HexStringToByteArray("314812FEBA22557AAD4AA2ABD20A817C7FAE8C4A761F737A18061BBFDEDAAEDF63264CF0F0AD731446E5074684C30F1A69B5317065C210AEC9BE6323B50165E2"), usr.PasswordHash);
            Assert.AreEqual(HelperMethods.HexStringToByteArray("E74B8C05BAF884A8F3AB951D3C7164F4C43A714ED344E97DE27C4150BA81CF0F3B46F6C70B92CEF016BB45D79FFA003B38FACC734BC342A2620E223DB634EE72"), usr.PasswordSalt);
        }

        [Test]
        public void GetNonExistingUser()
        {
            var userOps = new UserOperations(new TestExecutionContext(testData.UserIds[1], dbContext), new UserDataManager(dbContext));

            Assert.ThrowsAsync<UnauthorizedAccessException>(async () =>  await userOps.GetUserAsync<UserModel>(int.MaxValue));
        }

        [Test]
        public async Task RegisterUserOK()
        {
            var userOps = new UserOperations(new TestExecutionContext(null, null), new UserDataManager(dbContext));
            var usr = await userOps.RegisterUserAsync(new UserModel
            {
                Email = "newuser@test.pl",
                Name = "Registered user",
                Password = "test"
            });

            Assert.AreEqual("newuser@test.pl", usr.Email);
            Assert.AreEqual("Registered user", usr.Name);
            Assert.IsNull(usr.Password);
        }

        [Test]
        public void RegisterExistingUser()
        {
            var userOps = new UserOperations(new TestExecutionContext(null, null), new UserDataManager(dbContext));

            var ex = Assert.ThrowsAsync<BudgettoException>(async () => await userOps.RegisterUserAsync(new UserModel
            {
                Email = "test0@test.pl",
                Name = "Existing user",
                Password = "test"
            }));

            Assert.AreEqual("User already registered", ex.Errors.ErrorMessages[CommonConstValues.KeyForErrorsWithoutField].First());
        }

        [Test]
        public async Task LoginUserOK()
        {
            var userOps = new UserOperations(new TestExecutionContext(null, null), new UserDataManager(dbContext));
            var usr = await userOps.LoginUserAsync(new LoginModel
            {
                Email = "test0@test.pl",
                Password = "test"
            });

            Assert.AreEqual("test0@test.pl", usr.Email);
            Assert.AreEqual("User0", usr.Name);
            Assert.IsNull(usr.Password);
        }

        [Test]
        public void LoginUserWrongPassword()
        {
            var userOps = new UserOperations(new TestExecutionContext(null, null), new UserDataManager(dbContext));

            var ex = Assert.ThrowsAsync<BudgettoException>(async () =>  await userOps.LoginUserAsync(new LoginModel
            {
                Email = "test2@test.pl",
                Password = "wrong pwd"
            }));

            Assert.AreEqual(ErrorMessages.UserLoginFailed, ex.Errors.ErrorMessages[CommonConstValues.KeyForErrorsWithoutField].First());
        }

        [Test]
        public void LoginUserNonExistingUser()
        {
            var userOps = new UserOperations(new TestExecutionContext(null, null), new UserDataManager(dbContext));
            var ex = Assert.ThrowsAsync<BudgettoException>(async () => await userOps.LoginUserAsync(new LoginModel
            {
                Email = "nonexisting@test.pl",
                Password = "test"
            }));

            Assert.AreEqual(ErrorMessages.UserLoginFailed, ex.Errors.ErrorMessages[CommonConstValues.KeyForErrorsWithoutField].First());
        }
    }
}
